import { foodInMealMenu } from "./foodInMealMenu";
import { menu } from "./menu";

export class mealInMenu{
constructor(
public MealMenuId:number,public MenuID:number,public MealID :number,public OrdinalNumber ?:number,
public AlternativeAmountToChoose ?:number,
public FoodInMealMenu?:Array<foodInMealMenu>,
public Meals?:meal, public Menus?:menu,public maxChoose?:number ){}

}


export class meal{
  constructor(
  public MealID?:number,public MealName ?:string
  ,public MealInMenu?:Array<mealInMenu>,public isChecked?:boolean
  ){}
  }
