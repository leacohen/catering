import { foodInMealMenu } from "./foodInMealMenu";
import { productInFood } from "./productInFood";

export class food{
  constructor(
 public FoodID?:number,
 public FoodName?:string,
 public FoodPicture?:string
, public AbleToFreeze?:boolean,
 public NumDaysToMakeBefore?:number,
 public FoodInMealMenu?:Array<foodInMealMenu>,
 public ProductInFood?:Array<productInFood>,
 public isChecked?:boolean
 ) { }
}
