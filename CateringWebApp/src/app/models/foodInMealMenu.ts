import { food } from "./food";
import { mealInMenu } from "./mealInMenu";
import { orderDetails } from "./orderDetails";

export class foodInMealMenu {
constructor(
 public FoodInMealMenuID: number,
 public MealMenuId: number,
 public FoodID: number,
 public Foods?:food,
 public MealInMenu?:mealInMenu,
 public OrderDetails?:orderDetails

  ) {


  }
}


