import { EventType } from './eventType';

export class Event
{
     eventType:EventType;
     date:Date;
}
