import { Order } from "./order"

export class EventType
{
     EventID:number
     EventName:string
     Orders?:Array<Order>
}