import { dateSelectionJoinTransformer } from '@fullcalendar/core';
import { EventType } from './eventType';
import { menu } from './menu';
import { orderAddition } from './orderAddition';
import { orderDetails } from './orderDetails';

export class Order {
    OrderID:number;
    FirstName:string;
    LastName:string;
    Phone:string;
    CustomerEmail:string;
    OrderDate:Date;
    EventID?:number;
    EventDate:Date;
    EventTime?:Date;
    EventAddress:string;
    EventTypeID:number;
    MealsAmount:number;
    MenuID:number;
    OkByManager?:boolean;
    ClosedPrice?:number;
    PaiedBefore?:boolean;
    PaidBalance?:boolean;
    Comment?:string;
    EventTypes?:EventType;
    Menus?:menu;
    OrderAdditions?:Array<orderAddition>;
    OrderDetails?:Array<orderDetails>;

}
