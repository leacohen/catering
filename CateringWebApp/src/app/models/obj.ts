import { food } from './food';
import { foodInMealMenu } from './foodInMealMenu';
import { meal, mealInMenu } from './mealInMenu';
import { menu } from './menu';

export let listMenu:menu[]=[
  // (קוד תפריט
  //   שם תפריט
  //   מינימום מנות הזמנה
  //   מחיר למנה)
  new menu(1,"תפריט 1",20,80),
  new menu(2,"תפריט 2",30,55),
]
export let listMeal:meal[]=[
  // (קוד מנה
  //   שם מנה)
  new meal(1,"מנה 1"),
  new meal(2,"מנה 2"),
  new meal(3,"מנה 3"),
  new meal(4,"מנה 4"),

]
export let listMealInMenu:mealInMenu[]=[
  // (קוד מנה בתפריט
  //   קוד תפריט
  //   קוד מנה
  //   מספר סידורי
  //   כמות חלופות לבחירה)
  new mealInMenu(1,1,1,1,2),
  new mealInMenu(2,1,2,2,2),
  new mealInMenu(3,2,1,1,2),
  new mealInMenu(4,2,3,2,2),
]

export let listFood:food[]=[
  // (קוד מאכל
  //   שם מאכל
  //   תמונה
  //   ניתן להקפאה )כן/לא(
  //   מספר ימים להכנה מראש)
  new food(1,"אורז","",true,0),
  new food(2,"עוף","",true,0),
  new food(3,"דג סלומון","",true,0),
  new food(4,"אוכל 4","",true,0),
  new food(5,"אוכל 5","",true,0),
]

export let listFoodInMealMenu:foodInMealMenu[]=[
  // (קוד מאכל במנת תפריט
  //   קוד מנה בתפריט
  //   קוד מאכל)
  new foodInMealMenu(1,1,1),
  new foodInMealMenu(1,1,2),
  new foodInMealMenu(1,2,1),
  new foodInMealMenu(1,2,2),
]
