import { additions } from "./additions";
import { EventType } from "./eventType";
import { food } from "./food";
import { foodInMealMenu } from "./foodInMealMenu";
import { meal, mealInMenu } from "./mealInMenu";
import { menu } from "./menu";
import { Order } from "./order";
import { orderAddition } from "./orderAddition";
import { orderDetails } from "./orderDetails";
import { product } from "./product";
import { productInFood } from "./productInFood";
import { users } from "./users";

export  class db
{
    foodDB:Array<food>=new Array<food>();
    eventDB: Array<Event>= new Array<Event>();
    eventTypeDB:Array<EventType>=new Array<EventType>();
    additionsDB:Array<additions>=new Array<additions>();
    foodInMealMenuDB:Array<foodInMealMenu>=new Array<foodInMealMenu>();
    mealDB:Array<meal>=new Array<meal>();
    mealInMenuDB:Array<mealInMenu>=new Array<mealInMenu>();
    menuDb:Array<menu>=new Array<menu>();
    orderDb:Array<Order>=new Array<Order>();
    orderAdditionDB:Array<orderAddition>=new Array<orderAddition>();
    orderDetailsDB:Array<orderDetails>=new Array<orderDetails>();
    productDB:Array<product>=new Array<product>();
    productInFoodDB:Array<productInFood>=new Array<productInFood>();
    usersDB:Array<users>=new Array<users>();

    user=new users();
  isManager = false;

  constructor() {

    // this.foodDB = [
    //   { FoodID: 0, FoodName: 'לחמניה', AbleToFreeze: true, NumDaysToMakeBefore: 3,FoodPicture:'../../../../assets/image/dishes/לחמניה.PNG' },
    //   { FoodID: 1, FoodName: 'סלט פירות', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/סלט פירות.PNG' },
    //   { FoodID: 2, FoodName: 'מטבוחה', AbleToFreeze: true, NumDaysToMakeBefore: 2,FoodPicture:'../../../../assets/image/dishes/מטבוחה.PNG' },
    //   { FoodID: 3, FoodName: 'מרק כתום', AbleToFreeze: true, NumDaysToMakeBefore:1 ,FoodPicture:'../../../../assets/image/dishes/מרק כתום.PNG'},
    //   { FoodID: 4, FoodName: 'תפוחי אדמה', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/תפוחי אדמה.PNG' },
    //   { FoodID: 5, FoodName: 'דג סלמון', AbleToFreeze: true, NumDaysToMakeBefore: 10 ,FoodPicture:'../../../../assets/image/dishes/‏‏דג סלמון.PNG'},
    //   { FoodID: 6, FoodName: 'פסטה מוקרמת', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/‏‏פסטה מוקרמת.PNG' },
    //   { FoodID: 7, FoodName: 'סטייק', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/דג סלמון.PNG' },
    //   { FoodID: 8, FoodName: 'עוף אפוי', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/עוף אפוי.PNG' },
    //   { FoodID: 9, FoodName: 'שניצל', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/שניצל.PNG' },
    //   { FoodID: 10, FoodName: 'שעועית', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/שעועית.PNG' },
    //   { FoodID: 11, FoodName: 'סלט כרוב', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/סלט כרוב.PNG' },
    //   { FoodID: 12, FoodName: 'שייק פירות', AbleToFreeze: true, NumDaysToMakeBefore: 7 },
    //   { FoodID: 14, FoodName: 'סיגרים', AbleToFreeze: true, NumDaysToMakeBefore: 1 },
    //   { FoodID: 15, FoodName: 'פלפל ממולא', AbleToFreeze: true, NumDaysToMakeBefore: 3 },
    //   { FoodID: 16, FoodName: 'סלט גזר פיקנטי', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/סלט גזר פיקנטי.PNG' },
    //   { FoodID: 17, FoodName: 'טחינה', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/טחינה.PNG' },
    //   { FoodID: 18, FoodName: 'מוסקה', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/מוסקה.PNG' },
    //   { FoodID: 19, FoodName: 'עוגת רולדה', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/עוגת רולדה.PNG' },
    //   { FoodID: 20, FoodName: 'עוגת שוקולד עשירה', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/עוגת שוקולד עשירה.PNG' },
    //   { FoodID: 11, FoodName: 'קינוח פרווה בכוסות', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/קינוח פרווה בכוסות.PNG' },
    //   { FoodID: 11, FoodName: 'עודת גבינה ותותים בכוסיות', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/קינוח פרווה בכוסות.PNG' },
    //   { FoodID: 11, FoodName: 'בורקס גבינה', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/בורקס גבינה.PNG' },
    //   { FoodID: 7, FoodName: 'עוף בתנור', AbleToFreeze: false, NumDaysToMakeBefore: 1 },
    //   { FoodID: 8, FoodName: 'אורז', AbleToFreeze: false, NumDaysToMakeBefore: 1 },
    //   { FoodID: 9, FoodName: 'שניצל', AbleToFreeze: true, NumDaysToMakeBefore: 5 },
    //   { FoodID: 10, FoodName: 'חציל מטוגן', AbleToFreeze: false, NumDaysToMakeBefore: 1 },
    //   { FoodID: 11, FoodName: 'טחינה', AbleToFreeze: true, NumDaysToMakeBefore: 10 },
    // ];
    this.foodDB = [
      { FoodID: 0, FoodName: 'לחמניה', AbleToFreeze: true, NumDaysToMakeBefore: 3,FoodPicture:'../../../../assets/image/dishes/‏‏לחמניה.PNG' },
      { FoodID: 24, FoodName: 'סלט פירות', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/סלט פירות.PNG' },

      { FoodID: 1, FoodName: 'חציל מטוגן', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/חציל מטוגן.PNG'} ,
      { FoodID: 2, FoodName: 'טחינה', AbleToFreeze: true, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/טחינה.PNG' },
      { FoodID: 3, FoodName: 'סלט כרוב', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/סלט כרוב.PNG' },
      { FoodID: 4, FoodName: 'מטבוחה', AbleToFreeze: true, NumDaysToMakeBefore: 2,FoodPicture:'../../../../assets/image/dishes/מטבוחה.PNG' },
      { FoodID: 5, FoodName: 'סלט גזר פיקנטי', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/סלט גזר פיקנטי.PNG' },
      
      { FoodID: 6, FoodName: 'דג סלמון', AbleToFreeze: true, NumDaysToMakeBefore: 10 ,FoodPicture:'../../../../assets/image/dishes/‏‏דג סלמון.PNG'},
      { FoodID: 7, FoodName: 'מוסקה', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/מוסקה.PNG' },

      { FoodID: 8, FoodName: 'פוקאצה', AbleToFreeze: true, NumDaysToMakeBefore: 2,FoodPicture:'../../../../assets/image/dishes/פוקאצה.PNG' },
      { FoodID: 9, FoodName: 'פסטה מוקרמת', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/‏‏פסטה מוקרמת.PNG' },
      { FoodID: 10, FoodName: 'בורקס גבינה', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/בורקס גבינה.PNG' },

      { FoodID: 11, FoodName: 'מרק כתום', AbleToFreeze: true, NumDaysToMakeBefore:1 ,FoodPicture:'../../../../assets/image/dishes/מרק כתום.PNG'},
     
      { FoodID: 12, FoodName: 'עוף בתנור', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/‏‏עוף אפוי.PNG' },
      { FoodID: 13, FoodName: 'סטייק', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/פרגית.jpg' },
      { FoodID: 14, FoodName: 'שניצל', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/שניצל.PNG' },
     
      { FoodID: 15, FoodName: 'תפוחי אדמה', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/תפוחי אדמה.PNG' },
      { FoodID: 16, FoodName: 'שעועית', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/שעועית.PNG' },
      { FoodID: 17, FoodName: 'סיגרים', AbleToFreeze: true, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/סיגרים.PNG' },
      { FoodID: 18, FoodName: 'פלפל ממולא', AbleToFreeze: true, NumDaysToMakeBefore: 3,FoodPicture:'../../../../assets/image/dishes/פלפל ממולא.PNG' },
      { FoodID: 19, FoodName: 'אורז', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/אורז.PNG' },

      { FoodID: 20, FoodName: 'עוגת שוקולד עשירה', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/עוגת שוקולד עשירה.PNG' },
      { FoodID: 21, FoodName: 'קינוח פרווה בכוסות', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/קינוח פרווה בכוסות.PNG' },
      { FoodID: 22, FoodName: 'עוגת גבינה ותותים בכוסיות', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/עודת גבינה ותותים בכוסיות.PNG' },
      { FoodID: 23, FoodName: 'עוגת רולדה', AbleToFreeze: false, NumDaysToMakeBefore: 1,FoodPicture:'../../../../assets/image/dishes/עוגת רולדה.PNG' },
      { FoodID: 25, FoodName: 'שייק פירות', AbleToFreeze: true, NumDaysToMakeBefore: 7, FoodPicture:'../../../../assets/image/dishes/שייק פירות.PNG'},

    ];

    this.menuDb = [
      { MenuID: 0, MenuName: 'תפריט יוקרתי', MealPrice:150, MinMealNum: 10, favoraits: 50, title: 'לאנשים שבאמת מבינים', img: '../../../../assets/image/Menue/תפריט יוקרתי.PNG' },
      { MenuID: 1, MenuName: 'תפריט בסיסי', MealPrice: 80, MinMealNum: 50, favoraits: 2, title: 'כל מה שצריך לארוע', img: '../../../../assets/image/Menue/תפריט בסיסי.PNG' },
      { MenuID: 2, MenuName: 'תפריט חוץ', MealPrice: 100, MinMealNum: 100, favoraits: 225, title: 'הכי כיף לחגוג באויר הפתוח!!', img: '../../../../assets/image/Menue/תפריט חוץ.PNG' },
      { MenuID: 3, MenuName: 'תפריט חלבי', MealPrice: 120, MinMealNum: 15, favoraits: 86, title: 'לארועים שכיף להזמין וכיף להגיע', img: '../../../../assets/image/Menue/‏‏תפריט חלבי.PNG' },
    ];

    this.eventTypeDB = [
      { EventID: 0, EventName: 'חתונה' },
      { EventID: 1, EventName: 'ברית' },
      { EventID: 2, EventName: 'יום הולדת' },
      { EventID: 3, EventName: 'אירוסים' },
      { EventID: 4, EventName: 'בר מצווה' },
      { EventID: 5, EventName: 'אחר' },
    ];

    this.productDB = [
      { ProductID: 1, ProductName: 'בקבוקי מים' },
      { ProductID: 2, ProductName: 'בצל' },
      { ProductID: 3, ProductName: 'עגבניה' },
      { ProductID: 4, ProductName: 'ביצה' },
      { ProductID: 5, ProductName: 'בקבוקי שמן' },
      { ProductID: 6, ProductName: 'פלפל' },
      { ProductID: 7, ProductName: 'קילו מלח' },
      { ProductID: 8, ProductName: 'ראש שום' },
      { ProductID: 9, ProductName: 'כבד' },
      { ProductID: 10, ProductName: 'גרם סילאן' },
      { ProductID: 11, ProductName: 'בננה' },
      { ProductID: 12, ProductName: 'תפוז' },
      { ProductID: 13, ProductName: ' גרם חמוציות' },
      { ProductID: 14, ProductName: 'תפוח' },
      { ProductID: 15, ProductName: 'קופסת גלידונים' },
      { ProductID: 16, ProductName: 'כוס שמרים' },
      { ProductID: 17, ProductName: 'קילו קמח' },
      { ProductID: 18, ProductName: 'קילו תפוחי אדמה' },
      { ProductID: 19, ProductName: ' גרם פפריקה' },
      { ProductID: 20, ProductName: ' גרם אורז' },
      { ProductID: 21, ProductName: ' גרם שקדים פרוסים' },
      { ProductID: 22, ProductName: 'קילו חזה עוף' },
      { ProductID: 23, ProductName: ' עופות' },
      { ProductID: 24, ProductName: ' גרם פירורי לחם' },
      { ProductID: 25, ProductName: ' חציל' },
      { ProductID: 26, ProductName: ' גרם טחינה גולמית' },
      { ProductID: 27, ProductName: ' לימון' },
      { ProductID: 28, ProductName: ' גרם תות' },
      { ProductID: 29, ProductName: ' אפרסק' },
      { ProductID: 30, ProductName: ' גרם ריץ' },
      { ProductID: 31, ProductName: 'קופסת סיגרים' },
    ];

    this.productInFoodDB = [
      { ProductInFoodID: 0, FoodId: 0, ProductID: 1, AmountTo50Meals: 0.35 },
      { ProductInFoodID: 1, FoodId: 0, ProductID: 5, AmountTo50Meals: 0.2 },
      { ProductInFoodID: 2, FoodId: 0, ProductID: 4, AmountTo50Meals: 5 },
      { ProductInFoodID: 3, FoodId: 0, ProductID: 16, AmountTo50Meals: 0.33 },
      { ProductInFoodID: 4, FoodId: 0, ProductID: 17, AmountTo50Meals: 3 },
      { ProductInFoodID: 5, FoodId: 24, ProductID: 11, AmountTo50Meals: 20 },
      { ProductInFoodID: 6, FoodId: 24, ProductID: 12, AmountTo50Meals: 15 },
      { ProductInFoodID: 7, FoodId: 24, ProductID: 14, AmountTo50Meals: 20 },
      { ProductInFoodID: 8, FoodId: 24, ProductID: 13, AmountTo50Meals: 30 },
      { ProductInFoodID: 9, FoodId: 2, ProductID: 2, AmountTo50Meals: 15 },
      { ProductInFoodID: 10, FoodId: 2, ProductID: 3, AmountTo50Meals: 15 },
      { ProductInFoodID: 11, FoodId: 2, ProductID: 5, AmountTo50Meals: 2 },
      { ProductInFoodID: 12, FoodId: 2, ProductID: 6, AmountTo50Meals: 10 },
      { ProductInFoodID: 13, FoodId: 3, ProductID: 2, AmountTo50Meals: 15 },
      { ProductInFoodID: 14, FoodId: 3, ProductID: 5, AmountTo50Meals: 1 },
      { ProductInFoodID: 15, FoodId: 3, ProductID: 9, AmountTo50Meals: 5 },
      { ProductInFoodID: 16, FoodId: 3, ProductID: 10, AmountTo50Meals: 50 },
      { ProductInFoodID: 17, FoodId: 4, ProductID: 18, AmountTo50Meals: 8 },
      { ProductInFoodID: 18, FoodId: 4, ProductID: 19, AmountTo50Meals: 20 },
      { ProductInFoodID: 19, FoodId: 4, ProductID: 5, AmountTo50Meals: 2 },
      { ProductInFoodID: 20, FoodId: 5, ProductID: 15, AmountTo50Meals: 5 },
      { ProductInFoodID: 21, FoodId: 6, ProductID: 2, AmountTo50Meals: 15 },
      { ProductInFoodID: 22, FoodId: 6, ProductID: 3, AmountTo50Meals: 20 },
      { ProductInFoodID: 23, FoodId: 6, ProductID: 4, AmountTo50Meals: 50 },
      { ProductInFoodID: 24, FoodId: 6, ProductID: 5, AmountTo50Meals: 3 },
      { ProductInFoodID: 25, FoodId: 6, ProductID: 6, AmountTo50Meals: 10 },
      { ProductInFoodID: 26, FoodId: 6, ProductID: 7, AmountTo50Meals: 1 },
      { ProductInFoodID: 27, FoodId: 7, ProductID: 23, AmountTo50Meals: 40 },
      { ProductInFoodID: 28, FoodId: 7, ProductID: 19, AmountTo50Meals: 25 },
      { ProductInFoodID: 29, FoodId: 7, ProductID: 5, AmountTo50Meals: 1 },
      { ProductInFoodID: 30, FoodId: 7, ProductID: 7, AmountTo50Meals: 1 },
      { ProductInFoodID: 31, FoodId: 8, ProductID: 20, AmountTo50Meals: 300 },
      { ProductInFoodID: 32, FoodId: 8, ProductID: 21, AmountTo50Meals: 15 },
      { ProductInFoodID: 33, FoodId: 8, ProductID: 1, AmountTo50Meals: 2 },
      { ProductInFoodID: 34, FoodId: 9, ProductID: 22, AmountTo50Meals: 30 },
      { ProductInFoodID: 35, FoodId: 9, ProductID: 24, AmountTo50Meals: 20 },
      { ProductInFoodID: 36, FoodId: 9, ProductID: 5, AmountTo50Meals: 2 },
      { ProductInFoodID: 37, FoodId: 9, ProductID: 17, AmountTo50Meals: 2 },
      { ProductInFoodID: 38, FoodId: 10, ProductID: 25, AmountTo50Meals: 55 },
      { ProductInFoodID: 39, FoodId: 10, ProductID: 5, AmountTo50Meals: 5 },
      { ProductInFoodID: 40, FoodId: 11, ProductID: 26, AmountTo50Meals: 100 },
      { ProductInFoodID: 41, FoodId: 11, ProductID: 27, AmountTo50Meals: 5 },
      { ProductInFoodID: 42, FoodId: 11, ProductID: 1, AmountTo50Meals: 7 },
      { ProductInFoodID: 43, FoodId: 12, ProductID: 28, AmountTo50Meals: 30 },
      { ProductInFoodID: 44, FoodId: 12, ProductID: 29, AmountTo50Meals: 20 },
      { ProductInFoodID: 45, FoodId: 12, ProductID: 30, AmountTo50Meals: 15 },
      { ProductInFoodID: 46, FoodId: 12, ProductID: 11, AmountTo50Meals: 10 },
      { ProductInFoodID: 47, FoodId: 13, ProductID: 1, AmountTo50Meals: 6 },
      { ProductInFoodID: 48, FoodId: 13, ProductID: 2, AmountTo50Meals: 10 },
      { ProductInFoodID: 49, FoodId: 13, ProductID: 3, AmountTo50Meals: 10 },
      { ProductInFoodID: 50, FoodId: 13, ProductID: 5, AmountTo50Meals: 1 },
      { ProductInFoodID: 51, FoodId: 13, ProductID: 8, AmountTo50Meals: 4 },
      { ProductInFoodID: 52, FoodId: 13, ProductID: 16, AmountTo50Meals: 1 },
      { ProductInFoodID: 53, FoodId: 13, ProductID: 16, AmountTo50Meals: 1 },
      { ProductInFoodID: 54, FoodId: 13, ProductID: 17, AmountTo50Meals: 7 },
      { ProductInFoodID: 55, FoodId: 14, ProductID: 31, AmountTo50Meals: 6 },
      { ProductInFoodID: 56, FoodId: 14, ProductID: 5, AmountTo50Meals: 4 },
      { ProductInFoodID: 57, FoodId: 15, ProductID: 6, AmountTo50Meals: 55 },
      { ProductInFoodID: 58, FoodId: 15, ProductID: 3, AmountTo50Meals: 35 },
      { ProductInFoodID: 59, FoodId: 15, ProductID: 2, AmountTo50Meals: 20 },
      { ProductInFoodID: 60, FoodId: 15, ProductID: 1, AmountTo50Meals: 3 },
      { ProductInFoodID: 61, FoodId: 15, ProductID: 20, AmountTo50Meals: 10 },
    ];

    this.mealDB = [
      { MealID: 1, MealName: 'פתיחה' },
      { MealID: 2, MealName: 'ראשונה' },
      { MealID: 0, MealName: 'עיקרית' },
      { MealID: 3, MealName: 'קינוח' },
      { MealID: 4, MealName: 'סלט' },
      { MealID: 5, MealName: 'בר' },
      { MealID: 6, MealName: 'תוספות למנה עיקרית' },
    ];
    // this.mealDB = [
    //   { MealID: 3, MealName: 'עיקרית' },
    //   { MealID: 0, MealName: 'פתיחה' },
    //   { MealID: 1, MealName: 'ראשונה' },
    //   { MealID: 4, MealName: 'קינוח' },
    //   { MealID: 5, MealName: 'סלט' },
    //   { MealID: 6, MealName: 'בר' },
    // ];

    this.mealInMenuDB = [
      { MealMenuId: 0, MealID: 0, MenuID: 0 ,maxChoose:1},//יוקרתי עיקרית
      { MealMenuId: 1, MealID: 1, MenuID: 0,maxChoose:2 },//יוקרתי פתיחה
      { MealMenuId: 2, MealID: 2, MenuID: 0,maxChoose:2 },//יוקרתי ראשונה
      { MealMenuId: 3, MealID: 3, MenuID: 0,maxChoose:2 },//יוקרתי קינוח
      { MealMenuId: 4, MealID: 4, MenuID: 0,maxChoose:4 },//יוקרתי סלט
      { MealMenuId: 5, MealID: 5, MenuID: 0,maxChoose:1 },//יוקרתי בר
      { MealMenuId: 6, MealID: 0, MenuID: 1,maxChoose:1 },//בסיסי עיקרית
      { MealMenuId: 7, MealID: 2, MenuID: 1,maxChoose:1 },//בסיסי ראשונה
      { MealMenuId: 8, MealID: 3, MenuID: 1,maxChoose:1 },//בסיסי קינוח
      // {MealMenuId:9,MealID:4,MenuID:1},,maxChoose:1
      { MealMenuId: 10, MealID: 0, MenuID: 2,maxChoose:1 },//חוץ עיקרית
      { MealMenuId: 11, MealID: 1, MenuID: 2,maxChoose:1 },//חוץ פתיחה
      { MealMenuId: 12, MealID: 2, MenuID: 2,maxChoose:1 },//חוץ ראשונה
      { MealMenuId: 13, MealID: 3, MenuID: 2,maxChoose:1 },//חוץ קינוח
      { MealMenuId: 14, MealID: 4, MenuID: 2,maxChoose:3 },//חוץ סלט
      { MealMenuId: 15, MealID: 0, MenuID: 3,maxChoose:1 },//חלבי עיקרית
      // { MealMenuId: 16, MealID: 2, MenuID,maxChoose:1: 3 },//חלבי ראשונה
      { MealMenuId: 17, MealID: 3, MenuID: 3,maxChoose:1 },//חלבי קינוח
      { MealMenuId: 18, MealID: 4, MenuID: 3,maxChoose:2 }//חלבי סלט
    ];

    this.foodInMealMenuDB = [
      { FoodInMealMenuID: 0, FoodID: 3, MealMenuId: 0 },
      { FoodInMealMenuID: 1, FoodID: 24, MealMenuId: 1 },
      { FoodInMealMenuID: 2, FoodID: 6, MealMenuId: 2 },
      { FoodInMealMenuID: 3, FoodID: 1, MealMenuId: 3 },
      { FoodInMealMenuID: 4, FoodID: 5, MealMenuId: 3 },
      { FoodInMealMenuID: 5, FoodID: 0, MealMenuId: 1 },
      { FoodInMealMenuID: 6, FoodID: 2, MealMenuId: 4 },
      { FoodInMealMenuID: 7, FoodID: 5, MealMenuId: 5 },
      { FoodInMealMenuID: 8, FoodID: 1, MealMenuId: 5 },
      { FoodInMealMenuID: 9, FoodID: 4, MealMenuId: 6 },
      { FoodInMealMenuID: 10, FoodID: 0, MealMenuId: 7 },
      { FoodInMealMenuID: 11, FoodID: 5, MealMenuId: 8 },
      { FoodInMealMenuID: 12, FoodID: 3, MealMenuId: 10 },
      { FoodInMealMenuID: 13, FoodID: 1, MealMenuId: 11 },
      { FoodInMealMenuID: 14, FoodID: 6, MealMenuId: 12 },
      { FoodInMealMenuID: 15, FoodID: 1, MealMenuId: 13 },
      { FoodInMealMenuID: 16, FoodID: 5, MealMenuId: 13 },
      { FoodInMealMenuID: 17, FoodID: 2, MealMenuId: 14 },
      { FoodInMealMenuID: 18, FoodID: 4, MealMenuId: 15 },
      { FoodInMealMenuID: 19, FoodID: 6, MealMenuId: 15 },
      // { FoodInMealMenuID: 20, FoodID: 0, MealMenuId: 16 },
      { FoodInMealMenuID: 21, FoodID: 1, MealMenuId: 17 },
      { FoodInMealMenuID: 22, FoodID: 5, MealMenuId: 17 },
      { FoodInMealMenuID: 23, FoodID: 2, MealMenuId: 18 },
      { FoodInMealMenuID: 24, FoodID: 7, MealMenuId: 0 },
      { FoodInMealMenuID: 25, FoodID: 9, MealMenuId: 0 },
      { FoodInMealMenuID: 26, FoodID: 12, MealMenuId: 1 },
      { FoodInMealMenuID: 27, FoodID: 15, MealMenuId: 2 },
      { FoodInMealMenuID: 28, FoodID: 12, MealMenuId: 3 },
      { FoodInMealMenuID: 29, FoodID: 11, MealMenuId: 4 },
      { FoodInMealMenuID: 30, FoodID: 10, MealMenuId: 4 },
      { FoodInMealMenuID: 31, FoodID: 14, MealMenuId: 5 },
      { FoodInMealMenuID: 32, FoodID: 15, MealMenuId: 5 },
      { FoodInMealMenuID: 33, FoodID: 8, MealMenuId: 6 },
      { FoodInMealMenuID: 34, FoodID: 7, MealMenuId: 6 },
      { FoodInMealMenuID: 35, FoodID: 9, MealMenuId: 10 },
      { FoodInMealMenuID: 36, FoodID: 12, MealMenuId: 11 },
      { FoodInMealMenuID: 37, FoodID: 13, MealMenuId: 12 },
      { FoodInMealMenuID: 38, FoodID: 10, MealMenuId: 14 },
      { FoodInMealMenuID: 39, FoodID: 11, MealMenuId: 14 },
      { FoodInMealMenuID: 40, FoodID: 15, MealMenuId: 15 },
      { FoodInMealMenuID: 41, FoodID: 13, MealMenuId: 15 },
      { FoodInMealMenuID: 42, FoodID: 12, MealMenuId: 17 },
      { FoodInMealMenuID: 43, FoodID: 10, MealMenuId: 18 },
      { FoodInMealMenuID: 45, FoodID: 24, MealMenuId: 8 },

    ];

    this.orderDb=[
        {OrderID:0,OrderDate:new Date('01/01/2021'),EventDate:new Date('01/01/2021'),EventTypeID:0,FirstName:'בלימי',LastName:'פרקש',Phone:'0527171603',CustomerEmail:'bluma603@gmail.com',EventAddress:'הורדים 5 אשדוד',MealsAmount:250,MenuID:0,ClosedPrice:22500,OkByManager:true,PaiedBefore:true,PaidBalance:true},
        {OrderID:1,OrderDate:new Date('03/03/2021'),EventDate:new Date('03/14/2021'),EventTypeID:1,FirstName:'לאה',LastName:'כהן',Phone:'0548432540',CustomerEmail:'l0548432540@gmail.com',EventAddress:'בית כנסת באיאן ביתר',MealsAmount:50,MenuID:1,ClosedPrice:2700,OkByManager:false,PaiedBefore:false,PaidBalance:false},
        {OrderID:2,OrderDate:new Date('01/03/2021'),EventDate:new Date('03/08/2021'),EventTypeID:3,FirstName:'יעקב',LastName:'גמזו',Phone:'0546547896',CustomerEmail:'gamzu1@gmail.com',EventAddress:'נדבורנא 5 בני ברק',MealsAmount:40,MenuID:0,ClosedPrice:3635,OkByManager:false,PaiedBefore:false,PaidBalance:false},
        {OrderID:3,OrderDate:new Date('02/03/2021'),EventDate:new Date('03/11/2021'),EventTypeID:4,FirstName:'בילה',LastName:'פישר',Phone:'0533197770',CustomerEmail:'bila0533197770@gmail.com',EventAddress:'מהרשא 20 בית שמש',MealsAmount:100,MenuID:1,ClosedPrice:5500,OkByManager:true,PaiedBefore:true,PaidBalance:true},
        {OrderID:4,OrderDate:new Date('08/03/2021'),EventDate:new Date('03/14/2021'),EventTypeID:2,FirstName:'בילה',LastName:'פישר',Phone:'0533197770',CustomerEmail:'bila0533197770@gmail.com',EventAddress:'הכלנית 154 אשדוד',MealsAmount:30,MenuID:3,ClosedPrice:2250,OkByManager:true,PaiedBefore:true,PaidBalance:false},
        {OrderID:5,OrderDate:new Date('01/02/2021'),EventDate:new Date('04/20/2021'),EventTypeID:1,FirstName:'חדווה',LastName:'ליבוביץ',Phone:'025725471',CustomerEmail:'ch025725@gmail.com',EventAddress:'ניסנבאום 45 חולון',MealsAmount:150,MenuID:1,ClosedPrice:8400,OkByManager:true,PaiedBefore:true,PaidBalance:true},
        {OrderID:6,OrderDate:new Date('14/01/2021'),EventDate:new Date('03/22/2021'),EventTypeID:2,FirstName:'שירה',LastName:'הראל',Phone:'0587415255',CustomerEmail:'shira123@gmail.com',EventAddress:'זבוטינסקי 84 פתח תקווה',MealsAmount:50,MenuID:2,ClosedPrice:2465,OkByManager:true,PaiedBefore:true,PaidBalance:false},
        {OrderID:7,OrderDate:new Date('28/02/2021'),EventDate:new Date('03/01/2021'),EventTypeID:0,FirstName:'אלימלך',LastName:'שוורץ',Phone:'0533114574',CustomerEmail:'shvartz4574@gmail.com',EventAddress:'חזון איש 1 ביתר',MealsAmount:300,MenuID:1,ClosedPrice:17100,OkByManager:true,PaiedBefore:true,PaidBalance:false},

    ];

    this.orderDetailsDB = [
      { OrderDetailsID: 0, OrderID: 0, FoodInMealMenuID: 0, Amount: 250 },
      { OrderDetailsID: 1, OrderID: 0, FoodInMealMenuID: 1, Amount: 250 },
      { OrderDetailsID: 2, OrderID: 0, FoodInMealMenuID: 2, Amount: 250 },
      { OrderDetailsID: 3, OrderID: 0, FoodInMealMenuID: 3, Amount: 250 },
      { OrderDetailsID: 4, OrderID: 0, FoodInMealMenuID: 6, Amount: 250 },
      { OrderDetailsID: 5, OrderID: 0, FoodInMealMenuID: 7, Amount: 250 },
      { OrderDetailsID: 6, OrderID: 1, FoodInMealMenuID: 33, Amount: 50 },
      { OrderDetailsID: 7, OrderID: 1, FoodInMealMenuID: 10, Amount: 50 },
      { OrderDetailsID: 8, OrderID: 1, FoodInMealMenuID: 11, Amount: 50 },
      { OrderDetailsID: 6, OrderID: 2, FoodInMealMenuID: 24, Amount: 40 },
      { OrderDetailsID: 7, OrderID: 2, FoodInMealMenuID: 26, Amount: 40 },
      { OrderDetailsID: 8, OrderID: 2, FoodInMealMenuID: 27, Amount: 40 },
      { OrderDetailsID: 9, OrderID: 2, FoodInMealMenuID: 4, Amount: 40 },
      { OrderDetailsID: 29, OrderID: 2, FoodInMealMenuID: 1, Amount: 40 },
      { OrderDetailsID: 10, OrderID: 2, FoodInMealMenuID: 30, Amount: 40 },
      { OrderDetailsID: 11, OrderID: 2, FoodInMealMenuID: 32, Amount: 40 },
      { OrderDetailsID: 12, OrderID: 3, FoodInMealMenuID: 34, Amount: 100 },
      { OrderDetailsID: 13, OrderID: 3, FoodInMealMenuID: 10, Amount: 100 },
      { OrderDetailsID: 14, OrderID: 3, FoodInMealMenuID: 11, Amount: 100 },
      { OrderDetailsID: 15, OrderID: 4, FoodInMealMenuID: 41, Amount: 30 },
      { OrderDetailsID: 16, OrderID: 4, FoodInMealMenuID: 42, Amount: 30 },
      { OrderDetailsID: 17, OrderID: 4, FoodInMealMenuID: 43, Amount: 30 },
      { OrderDetailsID: 18, OrderID: 5, FoodInMealMenuID: 9, Amount: 150 },
      { OrderDetailsID: 19, OrderID: 5, FoodInMealMenuID: 10, Amount: 150 },
      { OrderDetailsID: 20, OrderID: 5, FoodInMealMenuID: 11, Amount: 150 },
      { OrderDetailsID: 21, OrderID: 6, FoodInMealMenuID: 35, Amount: 50 },
      { OrderDetailsID: 22, OrderID: 6, FoodInMealMenuID: 36, Amount: 50 },
      { OrderDetailsID: 23, OrderID: 6, FoodInMealMenuID: 37, Amount: 50 },
      { OrderDetailsID: 24, OrderID: 6, FoodInMealMenuID: 16, Amount: 50 },
      { OrderDetailsID: 25, OrderID: 6, FoodInMealMenuID: 17, Amount: 50 },
      { OrderDetailsID: 26, OrderID: 7, FoodInMealMenuID: 33, Amount: 300 },
      { OrderDetailsID: 27, OrderID: 7, FoodInMealMenuID: 10, Amount: 300 },
      { OrderDetailsID: 28, OrderID: 7, FoodInMealMenuID: 11, Amount: 300 },
      { OrderDetailsID: 28, OrderID: 7, FoodInMealMenuID: 24, Amount: 300 },
      { OrderDetailsID: 28, OrderID: 0, FoodInMealMenuID: 1, Amount: 250 },

    ];

    this.additionsDB=[
        {AdditionID:0,AdditionName:'מלצרים',PriceTo50Meals:100},
        {AdditionID:1,AdditionName:'קנדלברות',PriceTo50Meals:50},
        {AdditionID:2,AdditionName:'פרחים',PriceTo50Meals:35},
        {AdditionID:3,AdditionName:'זיקוקים',PriceTo50Meals:15}
    ];

    this.orderAdditionDB=[
      {OrderAddition:0,AdditionID:0,OrderID:7},
      {OrderAddition:1,AdditionID:0,OrderID:5},
      {OrderAddition:2,AdditionID:1,OrderID:7},
      {OrderAddition:3,AdditionID:1,OrderID:3},
      {OrderAddition:4,AdditionID:2,OrderID:2},
      {OrderAddition:5,AdditionID:3,OrderID:6},
    ]

    this.usersDB=[
      {UserID:0, FirstName:'בלימי',LastName:'פרקש',Mail:'bluma603@gmail.com',Phone:'0527171603'},
      {UserID:1, FirstName:'בילה',LastName:'פישר',Mail:'bila0533197770@gmail.com',Phone:'0533197770'},
      {UserID:2, FirstName:'לאה',LastName:'כהן',Mail:'l0548432540@gmail.com',Phone:'0548432540'},
      {UserID:3, FirstName:'יעקב',LastName:'גמזו',Mail:'gamzu1@gmail.com',Phone:'0546547896'},
      {UserID:4, FirstName:'חדווה',LastName:'ליבוביץ',Mail:'ch025725@gmail.com',Phone:'025725471'},
      {UserID:5, FirstName:'שירה',LastName:'הראל',Mail:'shira123@gmail.com',Phone:'0587415255'},
      {UserID:6, FirstName:'שוורץ',LastName:'שוורץ',Mail:'shvartz4574@gmail.com',Phone:'0533114574'},
    
    ]


  }
}
