import { food } from "./food"
import { product } from "./product"

export class productInFood
{
   ProductInFoodID:number
   FoodId :number
   ProductID:number
   AmountTo50Meals:number
   Foods?:food;
   Products?:product

   constructor(){}
}