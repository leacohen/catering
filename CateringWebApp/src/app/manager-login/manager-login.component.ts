import { Component, OnInit } from '@angular/core';
import { Manager } from '../models/manager';
import { LoginService } from '../services/login/login.service';

@Component({
  selector: 'app-manager-login',
  templateUrl: './manager-login.component.html',
  styleUrls: ['./manager-login.component.css']
})
export class ManagerLoginComponent implements OnInit {
  manager: Manager = new Manager();
  constructor(private loginService: LoginService) { }

  ngOnInit(): void {
    //מחיקה
    //this.loginService.deleteManager(6).subscribe(x=>alert("i delete manager "));

    //החזרת רשימת המנהלים
    // this.loginService.getAll().subscribe(x => {
    //   console.log(x);
    // });
  }
  //חיפוש לפי סיסמא
  onSubmit() {
    //debugger;
    this.manager.ManagerEmail = "";
    this.manager.MangerID = 0;
    this.loginService.searchManager(this.manager).subscribe(x => {
      console.log(x);
      this.loginService.managerLogin = x;
    });
  }
  //הוספה
  onSave() {
    this.loginService.addManager(this.manager).subscribe(x => {
      alert("succes");
      console.log(x);
      // this.loginService.managerLogin=x;
    });
  }
  //עידכון

}
