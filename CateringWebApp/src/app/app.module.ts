import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA  } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

//full calendar
import { FullCalendarModule } from '@fullcalendar/angular'; // the main connector. must go first
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
import interactionPlugin from '@fullcalendar/interaction';
import { CalendarModule } from './components/calendar/calendar.module';
import { MenusComponent } from './components/menus/menus.component';
import {  FoodsModule} from './components/foods/foods.module';
import {  OrderModule} from './components/order/order.module';
import { EventTypeComponent } from './components/event-type/event-type.component';
import { MenusTableComponent } from './components/menus/menus-table/menus-table.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import { NavComponent } from './components/Site/nav/nav.component';
import { MenuSelectComponent } from './components/menus/menu-select/menu-select.component';
import { NewMenueComponent } from './components/menus/new-menue/new-menue.component';
import { HeaderComponent } from './components/Site/header/header.component';
import { FooterComponent } from './components/Site/footer/footer.component';
import { HomeComponent } from './components/Site/home/home.component';
import { MatIconModule } from '@angular/material/icon';
import {MatTabsModule} from '@angular/material/tabs';
// import {MatInputModule} from '@angular/material/input';
import {MatBadgeModule} from '@angular/material/badge';
import { MenuuListToCostomerComponent } from './components/Customer/menuu-list-to-costomer/menuu-list-to-costomer.component';
import { MenuCComponent } from './components/Customer/menu-c/menu-c.component';
import { MakeAnOrderComponent } from './components/Customer/make-an-order/make-an-order.component';
import { AboutUsComponent } from './components/Site/about-us/about-us.component';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
//import { CalendarModule } from './components/calendar/calendar.module'; // a plugin
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatListModule } from '@angular/material/list';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { LoginClientComponent } from './components/Customer/login-client/login-client.component';
import { ManagerLoginComponent } from './manager-login/manager-login.component';
import { DishesToOrderComponent } from './components/Customer/dishes-to-order/dishes-to-order.component';
import { MyOrderComponent } from './components/Customer/my-order/my-order.component';
import { TheDishRecipeComponent } from './components/Manager/the-dish-recipe/the-dish-recipe.component';
import { MenusForFoodComponent } from './components/Manager/menus-for-food/menus-for-food.component';
import { DemandForGraphFoodComponent } from './components/Manager/demand-for-graph-food/demand-for-graph-food.component';
import { RecipesTableComponent } from './components/Manager/recipes-table/recipes-table.component';
import { ShoppingListComponent } from './components/Manager/shopping-list/shopping-list.component';
import {ChartModule} from 'primeng/chart';
import {MatStepperModule} from '@angular/material/stepper';

import {TableModule} from 'primeng/table';
import { OrderListComponent } from './components/Manager/order-list/order-list.component';
import { SingInCComponent } from './components/Customer/sing-in-c/sing-in-c.component';
import { MoreDetailsOrderComponent } from './components/Manager/more-details-order/more-details-order.component';
import { OrdersAwaitingConfirmationComponent } from './components/Manager/orders-awaiting-confirmation/orders-awaiting-confirmation.component';
import { EventTypeService } from './services/eventType/event-type.service';
import { FoodService } from './services/food/food.service';
import { LoginService } from './services/login/login.service';
import { MenuService } from './services/menu/menu.service';
import { OrderService } from './services/order/order.service';
import { ProductService } from './services/product/product.service';
import { db } from './models/db';
import { AddFoodComponent } from './components/Manager/add-food/add-food.component';
import { MenusListByDishComponent } from './components/Manager/menus-list-by-dish/menus-list-by-dish.component';
import { AlertsModule } from 'angular-alert-module';
import { TasksComponent } from './tasks/tasks.component';


FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin
]);

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA],
  declarations: [
    AppComponent,
    MenusComponent,
    EventTypeComponent,
    MenusTableComponent,
    NavComponent,
    MenuSelectComponent,
    NewMenueComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    MenuuListToCostomerComponent,
    MenuCComponent,
    MakeAnOrderComponent,
    AboutUsComponent,
    LoginClientComponent,
    ManagerLoginComponent,
    DishesToOrderComponent,
    MyOrderComponent,
    TheDishRecipeComponent,
    MenusForFoodComponent,
    DemandForGraphFoodComponent,
    RecipesTableComponent,
    ShoppingListComponent,
    OrderListComponent,
    SingInCComponent,
    MoreDetailsOrderComponent,
    OrdersAwaitingConfirmationComponent,
    AddFoodComponent,
    MenusListByDishComponent,
    TasksComponent
  ],
  imports: [
    AlertsModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FullCalendarModule,
    CalendarModule,
    FoodsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    OrderModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatStepperModule,
    MatTabsModule,
    MatIconModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatRadioModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    ChartModule,
    TableModule

  ],
  providers: [EventTypeService,FoodService,LoginService,MenuService, db
  ,OrderService,ProductService],
  bootstrap: [AppComponent]

})
export class AppModule { }
