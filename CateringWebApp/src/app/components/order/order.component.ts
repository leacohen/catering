import { Component,OnInit } from '@angular/core';
import { OrderService } from 'src/app/services/order/order.service';
import { Order } from '../../models/order';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { menu } from 'src/app/models/menu';
import { MenuService } from 'src/app/services/menu/menu.service';
import { EventTypeService } from 'src/app/services/eventType/event-type.service';
import { EventType } from 'src/app/models/eventType';
import { db } from 'src/app/models/db';
import { MoreDetailsOrderComponent } from '../Manager/more-details-order/more-details-order.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  constructor(public dialog: MatDialog,private DB:db) { }
  
  list:string[];
  myOrders=new Array<Order>();
  menuList=new Array<menu>();
  eventTypeList=new Array<EventType>();

  ngOnInit(): void
  {
    debugger;
    this.list = ['OrderDate','MealsAmount', 'MenuID', 'ClosedPrice', 'EventTypeID', 'OkByManager', 'PaiedBefore','PaidBalance','show'];

    this.myOrders=this.DB.orderDb.filter(o=>o.CustomerEmail==this.DB.user.Mail&&o.Phone==this.DB.user.Phone);

    this.menuList=this.DB.menuDb;
    this.eventTypeList=this.DB.eventTypeDB;
  
  }

  openDialog(orderID) {
    this.dialog.open(MoreDetailsOrderComponent,
      {
        data:{id:orderID}
      });
  }
  
  // addOrders()
  // {
  //   debugger;
  //       this.orderService.addOrder(this.order).subscribe(x=>{
  //       alert("succes");

  //    });
  // }
  // getAllOrders()
  // {
  //     this.orderService.getAllOrders().subscribe(x=>{
  //     // this.order=x;
  //     // this.eventList.push(x.EventID,x.)
  //     console.log(x);
  //     console.log(x)
  //   });
  // }
  // getOrderById(orderID:Order)
  // {
  //     this.orderService.getOrdersByID(orderID).subscribe(x=>{
  //     this.order=x;
  //     console.log(x)});
  // }
  // deleteOrder()
  // {
  //     this.orderService.deleteOrder(3).subscribe(x=>
  //      alert("ffff")
  //     );
  // }
  // //עדכון מאכל
  // updateOrder()
  // {
  //   // this.changeFood.FoodPicture=null;
  //    this.orderService.updateOrder(2,this.newOrder).subscribe(y=>
  //       alert("succes")
  //    );
  // }
}
