import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderComponent } from './order.component';
import { FormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule } from "@angular/forms";
import {MatTableModule} from '@angular/material/table';


@NgModule({
  declarations: [OrderComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatTableModule
  ],
  exports : [OrderComponent]

})
export class OrderModule { }
