import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product/product.service';
import { product } from '../../models/product'
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private productService:ProductService) { }
  newProduct:product=new product();
  id:number;
  productName:string;
  updatedId:number;
  updatedName:string;

  ngOnInit(): void {
    //מחיקה
    this.productService.deleteProduct(7).subscribe(x=>console.log('I delete product number- '));

   //החזרת הכל
   this.productService.getAllProducts().subscribe(x=>console.log(x));

}
     //הוספה
  onSave()
  {

    this.productService.addProduct(this.newProduct).subscribe(x=>{
    alert("i add new product")
     });
   }
  //החזרה לפי קוד
    onSearch()
    {
      this.productService.getProductById(this.id).subscribe(x=> this.productName=x.ProductName);
    }


     //עידכון
    onUpdate()
    {
      this.productService.updateProduct(this.updatedId,this.updatedName).subscribe(x=>{
        debugger;
       console.log(x);
       console.log("i update product id:"+this.updatedId);
       });
    }


}
