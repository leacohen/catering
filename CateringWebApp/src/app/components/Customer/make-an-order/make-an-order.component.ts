import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'angular-alert-module';
import { additions } from 'src/app/models/additions';
import { db } from 'src/app/models/db';
import { EventType } from 'src/app/models/eventType';
import { food } from 'src/app/models/food';
import { foodInMealMenu } from 'src/app/models/foodInMealMenu';
import { meal, mealInMenu } from 'src/app/models/mealInMenu';
import { menu } from 'src/app/models/menu';
import { Order } from 'src/app/models/order';
import { orderAddition } from 'src/app/models/orderAddition';
import { orderDetails } from 'src/app/models/orderDetails';
import { users } from 'src/app/models/users';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-make-an-order',
  templateUrl: './make-an-order.component.html',
  styleUrls: ['./make-an-order.component.css']
})
export class MakeAnOrderComponent implements OnInit {
  // selected="חתונה"
  // extras=['מלצרים','פרחים','שולחנות','כיסאות']

  newOrder = new Order();
  eventTypeList = new Array<EventType>();
  menuList = new Array<menu>();
  mealInMenuList = new Array<mealInMenu>();
  foodInMealMenuList = new Array<foodInMealMenu>();
  mealList = new Array<meal>();
  foodList = new Array<food>();
  additionsList = new Array<additions>();

  constructor(private alerts: AlertsService, public DB: db) { }

  ngOnInit(): void {
    debugger;
    this.eventTypeList = this.DB.eventTypeDB;
    this.menuList = this.DB.menuDb;
    this.mealInMenuList = this.DB.mealInMenuDB;
    this.mealList = this.DB.mealDB;
    this.foodInMealMenuList = this.DB.foodInMealMenuDB;
    this.foodList = this.DB.foodDB;
    this.additionsList = this.DB.additionsDB;

    this.newOrder.OrderAdditions = new Array<orderAddition>();
    this.newOrder.OrderDetails = new Array<orderDetails>();

    if (!this.DB.isManager) 
    {
      this.newOrder.FirstName = this.DB.user.FirstName
      this.newOrder.LastName = this.DB.user.LastName
      this.newOrder.Phone = this.DB.user.Phone
      this.newOrder.CustomerEmail = this.DB.user.Mail
    }
  }

  updatePrice() {
    debugger;
    if (this.newOrder.MenuID != undefined && this.newOrder.MealsAmount != undefined) {
      this.newOrder.ClosedPrice = this.newOrder.MealsAmount * this.menuList[this.newOrder.MenuID].MealPrice;
      let count = 0;
      this.newOrder.OrderAdditions.forEach(o => {
        count += o.Additions.PriceTo50Meals * Math.ceil(this.newOrder.MealsAmount / 50);
      })
      this.newOrder.ClosedPrice += count;
    }

  }

  resetOrderDetails() {
    this.newOrder.OrderDetails = new Array<orderDetails>();
  }

  addFoodToOrder(food: foodInMealMenu, meal: mealInMenu) {

    debugger;
 
    let orderDetail = new orderDetails();
    orderDetail.FoodInMealMenu = food;
    this.newOrder.OrderDetails.push(orderDetail)
    if (this.newOrder.OrderDetails.filter(o => o.FoodInMealMenu.MealMenuId === meal.MealMenuId).length>meal.maxChoose) {
      let index = this.newOrder.OrderDetails.findIndex(o => o.FoodInMealMenu.MealMenuId === meal.MealMenuId)
      this.newOrder.OrderDetails.splice(index, 1);
    }
  }
  isChecked(foodID, mealID) {
    debugger;

    return this.newOrder.OrderDetails.some(o=>o.FoodInMealMenu.FoodID==foodID&&this.mealInMenuList.find(m=>m.MealMenuId==o.FoodInMealMenu.MealMenuId).MealID==mealID)

  }

  addAdditionToOrder(addition: additions) {
    debugger;
    if (this.newOrder.OrderAdditions.some(o => o.Additions.AdditionID === addition.AdditionID)) {
      let index = this.newOrder.OrderAdditions.findIndex(o => o.Additions.AdditionID === addition.AdditionID);
      this.newOrder.OrderAdditions.splice(index, 1);
    }
    else {
      let orderAdd = new orderAddition();
      orderAdd.Additions = addition;
      this.newOrder.OrderAdditions.push(orderAdd);
    }
    this.updatePrice();
  }
  addOrder() {
    this.DB.user = new users();
    this.DB.user.Mail = this.newOrder.CustomerEmail;
    this.DB.user.Phone = this.newOrder.Phone;
    this.newOrder.OrderDate = new Date();
    this.DB.orderDb.push(this.newOrder);
    Swal.fire({
      icon: 'success',
      title: 'הזמנה נוצרה בהצלחה',
      showConfirmButton: false,
      timer: 1500
    })
    // this.alerts.setMessage('הזמנה נוצרה בהצלחה!','success');

  }
  counter(i: number) {
    return new Array(i);
  }
}
