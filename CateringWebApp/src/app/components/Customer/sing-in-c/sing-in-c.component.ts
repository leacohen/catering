import { Component, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { AlertsService } from 'angular-alert-module';
import { db } from 'src/app/models/db';
import { users } from 'src/app/models/users';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sing-in-c',
  templateUrl: './sing-in-c.component.html',
  styleUrls: ['./sing-in-c.component.css']
})
export class SingInCComponent implements OnInit {
  constructor(private alerts: AlertsService,private _bottomSheet: MatBottomSheet,private DB:db) { }
  firstFormGroup: any  ;
  secondFormGroup: any;
  thirdFormGroup: any;
  foureFormGroup: any;
  isLinear = false;
  password:string;
  hide = true;

  newUser=new users();

  ngOnInit(): void {

    this.newUser.Phone
  }

  sighIn()
  {
    debugger;
    this.DB.usersDB.push(this.newUser);
    // this.alerts.setMessage('נרשמת בהצלחה','success');
    Swal.fire({    
      icon: 'success',  
      title: 'נרשמת בהצלחה!',  
      showConfirmButton: false,  
      timer: 1500  
    })  

  }
}
