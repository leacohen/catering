import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingInCComponent } from './sing-in-c.component';

describe('SingInCComponent', () => {
  let component: SingInCComponent;
  let fixture: ComponentFixture<SingInCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingInCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingInCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
