import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuuListToCostomerComponent } from './menuu-list-to-costomer.component';

describe('MenuuListToCostomerComponent', () => {
  let component: MenuuListToCostomerComponent;
  let fixture: ComponentFixture<MenuuListToCostomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuuListToCostomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuuListToCostomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
