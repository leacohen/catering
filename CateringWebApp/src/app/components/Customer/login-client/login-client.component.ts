import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'angular-alert-module';
import { db } from 'src/app/models/db';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login-client',
  templateUrl: './login-client.component.html',
  styleUrls: ['./login-client.component.css']
})
export class LoginClientComponent implements OnInit {

  constructor(private alerts: AlertsService, private DB: db) { }

  mail = '';
  phone = '';
  password = '';
  checked = false;
  hide = true;
  ngOnInit(): void {
  }
  login() {
    if (this.password == '1234') {
      this.DB.isManager = true;
    }
    try {
      this.DB.user = this.DB.usersDB.find(u => u.Mail == this.mail)
      // debugger;
      // this.alerts.setMessage('התחברת בהצלחה, ברוך הבא!','success');
      if(this.DB.user)
      {
        Swal.fire({
          icon: 'success',
          title: 'ברוך הבא!',
          showConfirmButton: false,
          timer: 1500
        })
      }
      else{
        Swal.fire({
          icon: 'error',
          title: 'אינך רשום, יש להרשם ',
          showConfirmButton: false,
          timer: 1500
        })
      }
    
    }
    catch {
      Swal.fire({
        icon: 'error',
        title: 'הנתונים שגויים, לא ניתן להתחבר',
        showConfirmButton: false,
        timer: 1500
      })
    }
  }

}
