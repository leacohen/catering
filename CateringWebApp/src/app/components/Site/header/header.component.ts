import { Component, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatDialog } from '@angular/material/dialog';
import { db } from 'src/app/models/db';
import { LoginService } from 'src/app/services/login/login.service';
import { LoginClientComponent } from '../../Customer/login-client/login-client.component';
import { SingInCComponent } from '../../Customer/sing-in-c/sing-in-c.component';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private _bottomSheet: MatBottomSheet,public dialog: MatDialog,
    public DB:db,public loginService:LoginService) { }

    
  ngOnInit(): void {
  }
  openDialog() {
    this.dialog.open(LoginClientComponent);
  }
  openDialog2()
  {
    this.dialog.open(SingInCComponent);

  }
  openBottomSheetLogin(){
    this._bottomSheet.open(LoginClientComponent);
  }
  openBottomSheetSignin(){
    this._bottomSheet.open(SingInCComponent);

  }
}
