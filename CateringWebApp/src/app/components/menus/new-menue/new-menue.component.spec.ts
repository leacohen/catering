import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewMenueComponent } from './new-menue.component';

describe('NewMenueComponent', () => {
  let component: NewMenueComponent;
  let fixture: ComponentFixture<NewMenueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewMenueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewMenueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
