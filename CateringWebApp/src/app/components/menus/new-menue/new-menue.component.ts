import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertsService } from 'angular-alert-module';
import { db } from 'src/app/models/db';
import { food } from 'src/app/models/food';
import { foodInMealMenu } from 'src/app/models/foodInMealMenu';
import { meal, mealInMenu } from 'src/app/models/mealInMenu';
import { menu } from 'src/app/models/menu';
import Swal from 'sweetalert2';
import { AddFoodComponent } from '../../Manager/add-food/add-food.component';

@Component({
  selector: 'app-new-menue',
  templateUrl: './new-menue.component.html',
  styleUrls: ['./new-menue.component.css']
})
export class NewMenueComponent implements OnInit {

  constructor(private alerts: AlertsService, public dialog: MatDialog, private DB: db, private router: Router, private Activatedroute: ActivatedRoute) { }
  newMeal = new meal();
  addVisible = false;
  newMenu = new menu();
  mealsList = new Array<meal>();
  foodsList = new Array<food>();

  ngOnInit(): void {


    this.mealsList = this.DB.mealDB;
    this.foodsList = this.DB.foodDB;
    this.newMenu.MealInMenu = new Array<mealInMenu>();
    this.newMenu.img = "../../../../assets/image/Menue/placeholder-img.jpg";
    this.Activatedroute.params.subscribe(k => {
      let menuID = k["menuID"];
      if (menuID != -1) {
        if (this.newMenu.Orders == undefined) {
          this.newMenu = this.DB.menuDb.find(m => m.MenuID == menuID)
          this.newMenu.MealInMenu = this.DB.mealInMenuDB.filter(m => m.MenuID == menuID);
          this.newMenu.MealInMenu.forEach(m => m.FoodInMealMenu = this.DB.foodInMealMenuDB.filter(f => f.MealMenuId == m.MealMenuId))
          this.newMenu.MealInMenu.forEach(m => m.FoodInMealMenu.forEach(f => f.Foods = this.DB.foodDB.find(f2 => f2.FoodID == f.FoodID)))
          this.newMenu.MealInMenu.forEach(m => m.FoodInMealMenu.forEach(f => f.Foods.ProductInFood = this.DB.productInFoodDB.filter(p => p.FoodId == f.FoodID)))
          this.newMenu.MealInMenu.forEach(m => m.FoodInMealMenu.forEach(f => f.Foods.ProductInFood.forEach(p => p.Products = this.DB.productDB.find(p1 => p1.ProductID == p.ProductID))))
          this.mealsList.filter(m => this.newMenu.MealInMenu.some(mim => mim.MealID == m.MealID)).forEach(m => m.isChecked)
          debugger;
        }
      }
      debugger;
    })
    // this.newMenu.MealInMenu.forEach(m=>m.FoodInMealMenu=new Array<foodInMealMenu>())
  }

  isChecked(mealID) {
    return this.newMenu.MealInMenu.some(m => m.MealID == mealID);
  }
  isChecked2(foodID, mealID) {
    let index = this.newMenu.MealInMenu.findIndex(m => m.MealID == mealID);
    return this.newMenu.MealInMenu[index].FoodInMealMenu.some(f => f.FoodID == foodID);
  }
  addMealToMenu(meal: mealInMenu) {
    debugger;
    if (this.newMenu.MealInMenu.some(m => m.MealID === meal.MealID)) {
      let index = this.newMenu.MealInMenu.findIndex(m => m.MealID == meal.MealID);
      this.newMenu.MealInMenu.splice(index, 1);
    }
    else {
      meal.FoodInMealMenu = new Array<foodInMealMenu>();
      this.newMenu.MealInMenu.push(meal);
    }
  }

  addFoodToMealMenu(mealID: number, food: foodInMealMenu) {

    let mealIndex = this.newMenu.MealInMenu.findIndex(m => m.MealID == mealID);

    if (this.newMenu.MealInMenu[mealIndex].FoodInMealMenu.some(f => f.FoodID === food.FoodID)) {
      let index = this.newMenu.MealInMenu[mealIndex].FoodInMealMenu.findIndex(f => f.FoodID === food.FoodID);
      this.newMenu.MealInMenu[mealIndex].FoodInMealMenu.splice(index, 1);
    }
    else {
      // meal.FoodInMealMenu=new Array<foodInMealMenu>();
      this.newMenu.MealInMenu[mealIndex].FoodInMealMenu.push(food);
    }
  }
  openFileDialog() {
    let input = document.createElement('input');
    input.type = 'file';
    input.accept = "image/*"
    input.multiple = false;
    input.onchange = _ => {

      let files = Array.from(input.files);
      debugger;
      var reader = new FileReader();
      // this.audioPath = files;
      reader.readAsText(files[0]);
      reader.onload = (_event) => {
        reader.result;
        // this.newMenu.img= reader.result;
        debugger;
      }
    };
    input.click();

    this.setImg();
  }


  setImg() {
    setTimeout(() => {  
        this.newMenu.img = '../../../../assets/image/Menue/‏‏תפריט שבת.PNG';

     }, 3000);
    // this.newMenu.img = '../../../../assets/image/Menue/‏‏תפריט שבת.PNG';

  }

  addMeal() {

    this.newMeal.MealID = this.DB.mealDB.length;
    this.DB.mealDB.push(this.newMeal);
    this.newMeal = new meal();
    this.addVisible = false;

  }

  openDialog() {
    this.dialog.open(AddFoodComponent);
  }


  saveMenu() {
    debugger;
    if (!this.DB.menuDb.some(m => m.MenuID == this.newMenu.MenuID)) {
      this.newMenu.MenuID = this.DB.menuDb.length;
      this.newMenu.MealInMenu.forEach(m => {
        debugger;
        m.MealMenuId = this.DB.mealInMenuDB.length;
        let newMealMenu = new mealInMenu(this.DB.mealInMenuDB.length, this.newMenu.MenuID, m.MealID);
        this.DB.mealInMenuDB.push(newMealMenu);
        m.FoodInMealMenu.forEach(f => {
          f.FoodInMealMenuID = this.DB.foodInMealMenuDB.length;
          this.DB.foodInMealMenuDB.push(f);
        })
      })
      this.DB.menuDb.push(this.newMenu);

    }
    Swal.fire({
      icon: 'success',
      title: 'תפריט נשמר בהצלחה',
      showConfirmButton: false,
      timer: 1500
    })

  }
}
