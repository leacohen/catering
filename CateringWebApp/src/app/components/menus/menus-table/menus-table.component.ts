import { Component, OnInit } from '@angular/core';
import { menu } from 'src/app/models/menu';
import { db } from 'src/app/models/db';

@Component({
  selector: 'app-menus-table',
  templateUrl: './menus-table.component.html',
  styleUrls: ['./menus-table.component.css']
})
export class MenusTableComponent implements OnInit {

  constructor( private DB:db) { }
  menueList= new Array<menu>()
  ngOnInit(): void {
    this.menueList=this.DB.menuDb;

  }
  counter(i: number) {
    return new Array(i);
  }
}
