import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MenuService } from 'src/app/services/menu/menu.service';
import { menu } from '../../models/menu';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { listMenu, listMeal, listFood, listFoodInMealMenu, listMealInMenu } from '../../models/obj'
import { meal, mealInMenu } from 'src/app/models/mealInMenu';
import { food } from 'src/app/models/food';
import Swal from 'sweetalert2';
import { foodInMealMenu } from 'src/app/models/foodInMealMenu';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.css']
})
export class MenusComponent implements OnInit {
  id: number = 3;
  listMeal: meal[];

  menuName: string;
  minMealNum: number;
  mealPrice: number;

  mealName:string;

  selectedMealInmenu: number;
  foodByMealList: food[] = [];
  selectedFoodList: food[];
  constructor(private menuService: MenuService) { }
  foods = listFood
  MenueNameSelect = false;
  meals:any[]=[];

  selectedFoodsTomeal:food[]=[];
    selectedMealID:number;
  ngOnInit(): void {
    this.listMeal = listMeal;
    this.meals[0]={};

  }
  addMenu() {
    console.log(listMenu);
    listMenu.push(new menu(this.id, this.menuName, this.minMealNum, this.mealPrice));
    console.log(listMenu);
    this.id++;
  }

  saveFoodToMenue() {
    Swal.fire({
      // position: 'top-end',
      icon: 'success',
      title: 'המאכלים למנה ראשונה נשמרו בהצלחה!! ',
      showConfirmButton: false,
      timer: 2000
    })

  }

  finishnewMenue() {
    Swal.fire({
      // position: 'top-end',
      icon: 'success',
      title: 'התפריט שלך נשמר בהצלחה! ',
      showConfirmButton: false,
      timer: 2000
    })
  }

  addMeal() {
    listMeal.push(new meal(1, "111"));
  }

  setFoodByMealList() {
    // var a=listFood.filter()
    //רק מה שמתאים למנה הנבחרת listFoodInMealMenu-  מחזיר רשימת אוביקטים  של
    var a = listFoodInMealMenu.filter(fm => fm.MealMenuId == this.selectedMealInmenu);
    a.forEach(fm => {
      this.foodByMealList.push(listFood.find(f => f.FoodID == fm.FoodID));
    });
    // return this.foodByMealList;
    console.log(this.foodByMealList);

    // listFood.forEach(f=>{
    //   this.foodByMealList.push()
    // })
    // this.foodByMealList=a.filter(fbm=>fbm.FoodId==)
  }
  MenueNameSelect1(value:meal)
  {
    console.log(" MenueNameSelect1()");
    this.selectedMealInmenu=value.MealID;
    this.MenueNameSelect =!this.MenueNameSelect;
    this.setFoodByMealList()

  }
  // emailFormControl = new FormControl('', [
  //   Validators.required,
  //   Validators.email,
  // ]);


  // matcher = new ErrorStateMatcher();


  //#region  שליפה ממסד הנתונים

  // newMenu:menu=new menu();
  // updatedMenu:menu=new menu();
  // id:number;
  // iid:number;

  // //מחיקת תפריט
  // //this.menuService.deleteMenu(8).subscribe(x=>console.log("i delete menu"));

  //   //הצגת כל התפריטים
  //   this.menuService.getAllMenus().subscribe(x=>console.log(x));

  // }
  // //החזרת פרטי התפריט לפי קוד
  // getMenuDetails(id)
  //   {
  //     this.menuService.getMenuById(id).subscribe(x=>
  //       {
  //         console.log(x);
  //       });
  //   }
  //   //הוספת תפריט חדש
  // onSave()
  // {
  //   this.menuService.addMenu(this.newMenu).subscribe(x=>{
  //     console.log(x);
  //   alert("succses");
  //    });
  // }
  // //עריכת תפריט
  // updateMenu(id)
  // {
  //   this.menuService.updateMenu(id,this.updatedMenu).subscribe(x=>{
  //     debugger;
  //     console.log(x);
  //   });
  // }
  //#endregion
  addMealInMenu(){
    this.saveMeal();
this.meals.push();
  }
  saveMeal() {
    listMealInMenu.push(new mealInMenu(5,this.selectedMealID,1,1,3));
    this.selectedFoodsTomeal.forEach(food => {
      listFoodInMealMenu.push(new foodInMealMenu(2,5,food.FoodID))
    });
  }
}
