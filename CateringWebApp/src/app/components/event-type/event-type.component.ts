import { Component, OnInit } from '@angular/core';
import { EventTypeService } from 'src/app/services/eventType/event-type.service';
import { EventType } from '../../models/eventType';

@Component({
  selector: 'app-event-type',
  templateUrl: './event-type.component.html',
  styleUrls: ['./event-type.component.css']
})
export class EventTypeComponent implements OnInit {

  constructor(private eventTypeService:EventTypeService) { }

  eventType:EventType=new EventType();
  ngOnInit(): void {
  }

  addEventType()
  {
      this.eventTypeService.addEventType(this.eventType).subscribe(x=>{
        alert("succes");
        console.log(x);
     })
  }




}
