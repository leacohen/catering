import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableOfFoodsComponent } from './table-of-foods.component';

describe('TableOfFoodsComponent', () => {
  let component: TableOfFoodsComponent;
  let fixture: ComponentFixture<TableOfFoodsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableOfFoodsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableOfFoodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
