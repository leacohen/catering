import { Component, OnInit } from '@angular/core';
import { db } from 'src/app/models/db';
import { food } from 'src/app/models/food';
import { FoodService } from 'src/app/services/food/food.service';

@Component({
  selector: 'app-table-of-foods',
  templateUrl: './table-of-foods.component.html',
  styleUrls: ['./table-of-foods.component.css']
})
export class TableOfFoodsComponent implements OnInit {

  constructor(private foodService: FoodService , private db0:db) { }

  newFood:food;
  changeFood:food;
  foodsList:food[];
  list:string[];
  ngOnInit(): void {
    this.list = ['FoodName', 'FoodPicture', 'AbleToFreeze', 'NumDaysToMakeBefore'];
    this.foodsList= this.db0.foodDB;
    // this.foodService.getAllFood().subscribe(x => {//this.EntriesList=x;
    //   this.foodsList = x;
    //   console.log(x)
    // });
  }
  addFood() {
    this.newFood.FoodPicture = null;
    this.foodService.addFood(this.newFood).subscribe(x => {
      alert("succes");
      console.log(x);
    });
  }
  deleteFood() {
    this.foodService.deleteFood(5).subscribe(x =>
      alert("ffff")
    );
  }
  getFoodById() {
    this.foodService.getFoodById(3).subscribe(x => {
      console.log(x)
    });
  }
  //עדכון מאכל
  updateFood() {
    this.changeFood.FoodPicture = null;
    this.foodService.updateFood(2, this.newFood).subscribe(y =>
      alert("succes")
    );
  }

  getRecord(row)
  {
    console.log(row);
  }
}


//#region
// newFood:food=new food();
// changeFood:food=new food();
// foodsList:food[];
// list:string[];
// ngOnInit(): void {
//   this.list = ['FoodName', 'FoodPicture', 'AbleToFreeze', 'NumDaysToMakeBefore'];
//   this.foodService.getAllFood().subscribe(x => {//this.EntriesList=x;
//     this.foodsList = x;
//     console.log(x)
//   });
// }
// addFood() {
//   this.newFood.FoodPicture = null;
//   this.foodService.addFood(this.newFood).subscribe(x => {
//     alert("succes");
//     console.log(x);
//   });
// }
// deleteFood() {
//   this.foodService.deleteFood(5).subscribe(x =>
//     alert("ffff")
//   );
// }
// getFoodById() {
//   this.foodService.getFoodById(3).subscribe(x => {
//     console.log(x)
//   });
// }
// //עדכון מאכל
// updateFood() {
//   this.changeFood.FoodPicture = null;
//   this.foodService.updateFood(2, this.newFood).subscribe(y =>
//     alert("succes")
//   );
// }

// getRecord(row)
// {
//   console.log(row);
// }
//#endregion
