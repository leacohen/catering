import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TableOfFoodsComponent} from './table-of-foods/table-of-foods.component'
import { MatTableModule } from '@angular/material/table';
import { HttpClientModule } from '@angular/common/http';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule } from "@angular/forms";
@NgModule({
  declarations: [TableOfFoodsComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    MatTableModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  exports : [TableOfFoodsComponent]

})
export class FoodsModule { }


