import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CalendarOptions } from '@fullcalendar/angular';
import { title } from 'node:process';
import { db } from 'src/app/models/db';
import { OrderService } from 'src/app/services/order/order.service';
import { MoreDetailsOrderComponent } from '../Manager/more-details-order/more-details-order.component';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  
  // title = 'CateringWebApp';
  events;

  calendarOptions: CalendarOptions  = {
    initialView: 'dayGridMonth',
    dateClick: this.handleDateClick.bind(this), // bind is important!
     events:[],
    eventClick:this.handleEventClick.bind(this)
  };

  constructor(public dialog: MatDialog,private orderServ:OrderService,private DB:db) { }

  ngOnInit(): void {
 
  debugger;
  this.events=this.DB.orderDb.map(o=>{
    return { title:o.FirstName+" "+o.LastName, date:o.EventDate,OrderID:o.OrderID};
  })
   this.calendarOptions.events=this.events;
  }
  handleEventClick(arg)
  {
    this.openDialog(arg.event._def.extendedProps.OrderID);
   
  }

  openDialog(orderID) {
    this.dialog.open(MoreDetailsOrderComponent,
      {
        data:{id:orderID}
      });
  }
  handleDateClick(arg) {

    //alert('date click! ' + arg.dateStr)
    // alert( "פה נפרט את הארוע שיתקיים ביום זה");
  }
  toggleWeekends() {
    this.calendarOptions.weekends = !this.calendarOptions.weekends // toggle the boolean!
  }
}
