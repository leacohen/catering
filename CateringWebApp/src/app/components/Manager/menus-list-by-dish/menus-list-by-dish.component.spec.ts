import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenusListByDishComponent } from './menus-list-by-dish.component';

describe('MenusListByDishComponent', () => {
  let component: MenusListByDishComponent;
  let fixture: ComponentFixture<MenusListByDishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenusListByDishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenusListByDishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
