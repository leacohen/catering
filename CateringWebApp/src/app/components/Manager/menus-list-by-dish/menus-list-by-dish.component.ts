import { mealInMenu } from 'src/app/models/mealInMenu';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { menu } from 'src/app/models/menu';
import { db } from 'src/app/models/db';
import { foodInMealMenu } from 'src/app/models/foodInMealMenu';

import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';




@Component({
  selector: 'app-menus-list-by-dish',
  templateUrl: './menus-list-by-dish.component.html',
  styleUrls: ['./menus-list-by-dish.component.css']
})
export class MenusListByDishComponent implements OnInit {



  constructor(private DB: db,@Inject(MAT_DIALOG_DATA) public data:any) {}
  menusList = new Array<menu>();
  foodInMealMenuList = new Array<foodInMealMenu>();
  mealInMenuList = new Array<mealInMenu>();
  mmList=new Array<mealInMenu>();
  menusByDishes=new Array<menu>();
  foodId:number
  title:string;
  ngOnInit(): void {
    this.foodId=this.data.id;
    this.menusList = this.DB.menuDb;
    this.foodInMealMenuList = this.DB.foodInMealMenuDB;
    this.mealInMenuList = this.DB.mealInMenuDB;
    this.getMenusListByDish(this.foodId);
    this.title=' תפריטים בהם כלול המאכל '+ this.data.name;
  }

  getMenusListByDish(foodID: number) {
    debugger;
    let xx = this.foodInMealMenuList.filter(fmm => fmm.FoodID == foodID);
    this.mmList= this.mealInMenuList.filter(m=>xx.some(f=>f.MealMenuId==m.MealMenuId));
    this.menusByDishes= this.menusList.filter(m=>this.mmList.some(mm=>mm.MenuID==m.MenuID))

    // xx.forEach(f => {
    //   let helper = f.MealMenuId
    //   this.mmList = this.mealInMenuList.filter(mm => mm.MealMenuId == helper);
    // })
    // this.mmList.forEach(mmt => {
    //  this.menusByDishes.push(this.menusList.find(m=>m.MenuID==mmt.MenuID));
    // });
  }
  counter(i: number) {
    return new Array(i);
  }
  // closeDialog() {
  //   this.dialogRef.close({ event: 'close' });
  // }
}
