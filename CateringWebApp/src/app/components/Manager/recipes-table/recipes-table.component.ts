import { element } from 'protractor';
import { debugOutputAstAsTypeScript } from '@angular/compiler';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { db } from 'src/app/models/db';
import { food } from 'src/app/models/food';
import { FoodService } from 'src/app/services/food/food.service';
import { MenusListByDishComponent } from '../menus-list-by-dish/menus-list-by-dish.component';
import { TheDishRecipeComponent } from '../the-dish-recipe/the-dish-recipe.component';
import { DemandForGraphFoodComponent } from '../demand-for-graph-food/demand-for-graph-food.component';

@Component({
  selector: 'app-recipes-table',
  templateUrl: './recipes-table.component.html',
  styleUrls: ['./recipes-table.component.css']
})
export class RecipesTableComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = ['FoodPicture', 'FoodName', 'AbleToFreeze', 'NumDaysToMakeBefore', 'btn1', 'btn2', 'btn3'];

  dataSource: MatTableDataSource<food>;

  @ViewChild(MatPaginator) paginator: MatPaginator;


  constructor(private foodServ: FoodService, public dialog: MatDialog, private DB: db) {
    // foodServ.getAllFood().subscribe(f=>{
    //   debugger;
    // this.dataSource = new MatTableDataSource<food>(f);
    // }
    // )
    this.dataSource = new MatTableDataSource<food>(DB.foodDB);

  }

  ngOnInit() {

  }


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }


  openDialog(element): void {
    const dialogRef = this.dialog.open(MenusListByDishComponent, {
      width: '700px',
      data: {
        id: element.FoodID,
        name:element.FoodName
      }
    });
  }

  openDialog2(elemnt){
    const dialogRef =this.dialog.open(TheDishRecipeComponent, {
      width: '500px',
      data: {
        id: elemnt.FoodID,
        name:elemnt.FoodName,
        // img:element.FoodPicture
      }
    });
  }
  openDialog3(elemnt){
    const dialogRef =this.dialog.open(DemandForGraphFoodComponent, {
      width: '900px',
      data: {
        id: elemnt.FoodID,
        name:elemnt.FoodName
      }
    });
  }
}

export interface PeriodicElement {
  image?: any,
  name: string;
  position: number;
  weight: number;
  symbol: string;
  btn1?: any;
  btn2?: any;
  btn3?: any;

}

const ELEMENT_DATA: PeriodicElement[] = [
  { image: '', position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H', btn1: '', btn2: '', btn3: '' },
  { image: '', position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H', btn1: '', btn2: '', btn3: '' },
  { image: '', position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H', btn1: '', btn2: '', btn3: '' },
  { image: '', position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H', btn1: '', btn2: '', btn3: '' },
  { image: '', position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H', btn1: '', btn2: '', btn3: '' },
  { image: '', position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H', btn1: '', btn2: '', btn3: '' },
  { image: '', position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H', btn1: '', btn2: '', btn3: '' },
  { image: '', position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H', btn1: '', btn2: '', btn3: '' },

  // {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  // {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  // {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  // {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  // {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  // {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  // {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  // {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  // {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  // {position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na'},
  // {position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg'},
  // {position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al'},
  // {position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si'},
  // {position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P'},
  // {position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S'},
  // {position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl'},
  // {position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar'},
  // {position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K'},
  // {position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca'},
];
