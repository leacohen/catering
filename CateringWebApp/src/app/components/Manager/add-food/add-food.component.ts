import { Component, OnInit } from '@angular/core';
import { AlertsService } from 'angular-alert-module';
import { db } from 'src/app/models/db';
import { food } from 'src/app/models/food';
import { product } from 'src/app/models/product';
import { productInFood } from 'src/app/models/productInFood';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-food',
  templateUrl: './add-food.component.html',
  styleUrls: ['./add-food.component.css']
})
export class AddFoodComponent implements OnInit {

  newProduct=new product();
  newFood=new food();
  addVisible=false;
  productList=new Array<product>();
  constructor(private DB:db,private alerts: AlertsService) { }

  ngOnInit(): void {
    this.productList=this.DB.productDB;
    this.newFood.ProductInFood= new Array<productInFood>();
  }

  addproductToFood(product:productInFood)
  {
    debugger;
    if(this.newFood.ProductInFood.some(p=>p.ProductID===product.ProductID))
      {
        let index=this.newFood.ProductInFood.findIndex(p=>p.ProductID===product.ProductID);
        this.newFood.ProductInFood.splice(index,1);
        this.newFood.ProductInFood[0].AmountTo50Meals
      }
      else
      {
        this.newFood.ProductInFood.push(product);
      }
  }

  addProduct()
  {
    this.newProduct.ProductID=this.DB.productDB.length+1;
    this.DB.productDB.push(this.newProduct);
    this.newProduct=new product();
    this.addVisible=false;
  }
  save()
  {
    this.newFood.FoodID=this.DB.foodDB.length;
    this.DB.foodDB.push(this.newFood);
    Swal.fire({    
      icon: 'success',  
      title: 'מאכל נוסף בהצלחה',  
      showConfirmButton: false,  
      timer: 1500  
    }) 
    // this.alerts.setMessage('מאכל נוסף בהצלחה','success');

  }
}
