import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { db } from 'src/app/models/db';
import { Order } from 'src/app/models/order';

@Component({
  selector: 'app-more-details-order',
  templateUrl: './more-details-order.component.html',
  styleUrls: ['./more-details-order.component.css']
})
export class MoreDetailsOrderComponent implements OnInit {

  constructor(private DB: db, @Inject(MAT_DIALOG_DATA) public data: any) { }

  order = new Order();
  ngOnInit(): void {

    debugger;
    this.order = this.DB.orderDb.find(o => o.OrderID == this.data.id)
    if (this.order.EventTypes == undefined) {
      this.order.EventTypes = this.DB.eventTypeDB.find(e => e.EventID == this.order.EventTypeID);
      this.order.Menus = this.DB.menuDb.find(m => m.MenuID == this.order.MenuID);
      this.order.OrderAdditions = this.DB.orderAdditionDB.filter(o => o.OrderID == this.order.OrderID);
      this.order.OrderAdditions.forEach(o => o.Additions = this.DB.additionsDB.find(a => a.AdditionID == o.AdditionID));
      this.order.OrderDetails = this.DB.orderDetailsDB.filter(o => o.OrderID == this.order.OrderID);
      this.order.OrderDetails.forEach(o => {
        o.FoodInMealMenu = this.DB.foodInMealMenuDB.find(f => f.FoodInMealMenuID == o.FoodInMealMenuID);
        o.FoodInMealMenu.Foods = this.DB.foodDB.find(f => f.FoodID == o.FoodInMealMenu.FoodID);
        o.FoodInMealMenu.MealInMenu = this.DB.mealInMenuDB.find(m => m.MealMenuId == o.FoodInMealMenu.MealMenuId);
        o.FoodInMealMenu.MealInMenu.Meals = this.DB.mealDB.find(m => m.MealID == o.FoodInMealMenu.MealInMenu.MealID);

      })
    }
  }
}
