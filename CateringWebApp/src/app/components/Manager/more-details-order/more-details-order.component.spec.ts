import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoreDetailsOrderComponent } from './more-details-order.component';

describe('MoreDetailsOrderComponent', () => {
  let component: MoreDetailsOrderComponent;
  let fixture: ComponentFixture<MoreDetailsOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoreDetailsOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoreDetailsOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
