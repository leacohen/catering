import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { title } from 'node:process';
import { db } from 'src/app/models/db';
import { EventType } from 'src/app/models/eventType';
import { Event } from 'src/app/models/event';
import { Order } from 'src/app/models/order';

@Component({
    selector: 'app-demand-for-graph-food',
    templateUrl: './demand-for-graph-food.component.html',
    styleUrls: ['./demand-for-graph-food.component.css']
})
export class DemandForGraphFoodComponent implements OnInit {

    title: string;
    //   data: any;

    constructor(private DB: db, @Inject(MAT_DIALOG_DATA) public data: any) {
        //   this.data = {
        //       labels: ['אירוסין', 'בר מצווה ', 'חתונה', 'ברית', 'יום הולדת ', 'חינה', 'שבת חתן '],
        //       datasets: [
        //           {
        //               label: 'My First dataset',
        //               backgroundColor: '#FFD700',
        //               borderColor: '#FFD700',
        //               data: [65, 59, 80, 81, 56, 55, 40]
        //           },
        //           {
        //               label: 'My Second dataset',
        //               backgroundColor: '#black',
        //               borderColor: '#7CB342',
        //               data: [28, 48, 40, 19, 86, 27, 90]
        //           }
        //       ]
        //   }
    }
    colors=['#000','#FFD700','#3df80f','#0ff8ec','#0f1ef8','#e50ff8','#f80f2e']
    basicData: any = {
        labels: ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני'],
        datasets: [
            // {
            //     label: 'First Dataset',
            //     data: [65, 59, 80, 81, 56, 55, 40],
            //     fill: false,
            //     borderColor: '#42A5F5'
            // },
            // {
            //     label: 'Second Dataset',
            //     data: [28, 48, 40, 19, 86, 27, 90],
            //     fill: false,
            //     borderColor: '#FFA726'
            // }
        ]
    };
    basicOptions: any
    eventTypeList = new Array<EventType>();
    orderList = new Array<Order>();
    ngOnInit(): void {
        debugger;
        this.title = 'גרף ביקוש למאכל ' + this.data.name;
        this.eventTypeList = this.DB.eventTypeDB;
        this.orderList = this.DB.orderDb;
        this.orderList.forEach(o => {
            o.OrderDetails = this.DB.orderDetailsDB.filter(od => od.OrderID == o.OrderID);
            o.OrderDetails.forEach(od => {
                od.FoodInMealMenu = this.DB.foodInMealMenuDB.find(f => f.FoodInMealMenuID == od.FoodInMealMenuID);
                // od.FoodInMealMenu.Foods=this.DB.foodDB.find(f=>f.FoodID==od.FoodInMealMenu.FoodID);
            })
        })
        // this.basicOptions.datasets = new Array<any>();
        this.eventTypeList.forEach((et, i) => {
            this.basicData.datasets.push({
                label: et.EventName,
                data: [0, 1, 2, 3, 4, 5].map(month => {
                    return this.orderList.filter(o => o.OrderDetails.some(od => od.FoodInMealMenu.FoodID == this.data.id) && o.EventTypeID == et.EventID && o.EventDate.getMonth() == month).length
                }),
                fill: false,
                borderColor: this.colors[i]
            })
        })
        debugger;
        // this.basicData = {
        //     labels: ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי'],
        //     datasets: [
        //         {
        //             label: 'First Dataset',
        //             data: [65, 59, 80, 81, 56, 55, 40],
        //             fill: false,
        //             borderColor: '#42A5F5'
        //         },
        //         {
        //             label: 'Second Dataset',
        //             data: [28, 48, 40, 19, 86, 27, 90],
        //             fill: false,
        //             borderColor: '#FFA726'
        //         }
        //     ]
        // }


    }
}
