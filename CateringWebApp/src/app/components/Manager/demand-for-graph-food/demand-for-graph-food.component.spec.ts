import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandForGraphFoodComponent } from './demand-for-graph-food.component';

describe('DemandForGraphFoodComponent', () => {
  let component: DemandForGraphFoodComponent;
  let fixture: ComponentFixture<DemandForGraphFoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemandForGraphFoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemandForGraphFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
