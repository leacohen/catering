import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AlertsService } from 'angular-alert-module';
import { db } from 'src/app/models/db';
import { Order } from 'src/app/models/order';
import { OrderService } from 'src/app/services/order/order.service';
import Swal from 'sweetalert2';
import { MoreDetailsOrderComponent } from '../more-details-order/more-details-order.component';

@Component({
  selector: 'app-orders-awaiting-confirmation',
  templateUrl: './orders-awaiting-confirmation.component.html',
  styleUrls: ['./orders-awaiting-confirmation.component.css']
})
export class OrdersAwaitingConfirmationComponent implements OnInit {

  awaitingOrders=new Array<Order>();
  constructor(private alerts: AlertsService,public dialog: MatDialog,private orderServ:OrderService,private DB:db) { }

  ngOnInit(): void {

    this.awaitingOrders=this.DB.orderDb.filter(o=>o.OkByManager!=true);
    // this.orderServ.getAaitingOrders().subscribe(o=>{
    //   debugger;
    //   this.awaitingOrders=o;
    // })
  }

  ok(orderID:number)
  {
    this.DB.orderDb.find(o=>o.OrderID===orderID).OkByManager=true;
    let index= this.awaitingOrders.findIndex(o=>o.OrderID===orderID);
    this.awaitingOrders.splice(index,1);
    Swal.fire({   
      icon: 'success',  
      title: 'ההזמנה אושרה!',  
      showConfirmButton: false,  
      timer: 1500  
    }) 
    // this.alerts.setMessage('ההזמנה אושרה!','success');

    debugger;
  }

  openDialog(orderID) {
    this.dialog.open(MoreDetailsOrderComponent,
      {
        data:{id:orderID}
      });
  }
  
}
