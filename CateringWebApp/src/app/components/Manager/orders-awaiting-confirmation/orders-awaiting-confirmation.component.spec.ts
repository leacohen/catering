import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersAwaitingConfirmationComponent } from './orders-awaiting-confirmation.component';

describe('OrdersAwaitingConfirmationComponent', () => {
  let component: OrdersAwaitingConfirmationComponent;
  let fixture: ComponentFixture<OrdersAwaitingConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersAwaitingConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersAwaitingConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
