import { Component, OnInit } from '@angular/core';
import { db } from 'src/app/models/db';
import { orderDetails } from 'src/app/models/orderDetails';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {

  shoppingList = new Map<string, number>();
  constructor(private DB: db) {
    this.setShoppingList();
  }

  ngOnInit(): void {

  }

  setShoppingList() {

    let helpDate = new Date();
    helpDate.setDate(helpDate.getDate() + 7)
    let helper = this.DB.orderDb.filter(o => o.EventDate >= new Date() && o.EventDate < helpDate);
    helper.forEach(o => {
      o.OrderDetails = this.DB.orderDetailsDB.filter(od => od.OrderID == o.OrderID);
      o.OrderDetails.forEach(od => {
        od.FoodInMealMenu = this.DB.foodInMealMenuDB.find(f => f.FoodInMealMenuID == od.FoodInMealMenuID);
        od.FoodInMealMenu.Foods = this.DB.foodDB.find(f => f.FoodID == od.FoodInMealMenu.FoodID);
        od.FoodInMealMenu.Foods.ProductInFood = this.DB.productInFoodDB.filter(p => p.FoodId == od.FoodInMealMenu.FoodID)
        od.FoodInMealMenu.Foods.ProductInFood.forEach(p => p.Products = this.DB.productDB.find(p1 => p1.ProductID == p.ProductID))
      })
      o.OrderDetails.forEach(od => {
        od.FoodInMealMenu.Foods.ProductInFood.forEach(p => {
          if (this.shoppingList.has(p.Products.ProductName))
          this.shoppingList.set(p.Products.ProductName,this.shoppingList.get(p.Products.ProductName)+ p.AmountTo50Meals / 50 * od.Amount);

            // this.shoppingList[p.Products.ProductName]=this.shoppingList.get(p.Products.ProductName)+ p.AmountTo50Meals / 50 * od.Amount;
          else
            this.shoppingList.set(p.Products.ProductName, p.AmountTo50Meals / 50 * od.Amount);
        })
      })
    })

    debugger;
  }

  print() {
    const printContent = document.getElementById("shoppingList");
    const WindowPrt = window.open('', '', 'left=0,top=0,width=900,height=900,toolbar=0,scrollbars=0,status=0');
    WindowPrt.document.write(printContent.innerHTML);
    WindowPrt.document.close();
    WindowPrt.focus();
    WindowPrt.print();
    WindowPrt.close();

  }
}
