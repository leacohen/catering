import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { db } from 'src/app/models/db';
import { food } from 'src/app/models/food';
import { product } from 'src/app/models/product';
import { productInFood } from 'src/app/models/productInFood';

@Component({
  selector: 'app-the-dish-recipe',
  templateUrl: './the-dish-recipe.component.html',
  styleUrls: ['./the-dish-recipe.component.css']
})
export class TheDishRecipeComponent implements OnInit {

  constructor(private DB: db,@Inject(MAT_DIALOG_DATA) public data:any) {
   }

  productList=new Array<product>();
  productInFoodList=new Array<productInFood>();

  ngOnInit(): void {
    console.log(this.data);
    this.productList=this.DB.productDB;
    this.productInFoodList=this.DB.productInFoodDB.filter(p=>p.FoodId==this.data.id);
    debugger;
    // this.productInFoodList[0].ProductID
  }

}
