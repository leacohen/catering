import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TheDishRecipeComponent } from './the-dish-recipe.component';

describe('TheDishRecipeComponent', () => {
  let component: TheDishRecipeComponent;
  let fixture: ComponentFixture<TheDishRecipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TheDishRecipeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TheDishRecipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
