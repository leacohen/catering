import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenusForFoodComponent } from './menus-for-food.component';

describe('MenusForFoodComponent', () => {
  let component: MenusForFoodComponent;
  let fixture: ComponentFixture<MenusForFoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenusForFoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenusForFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
