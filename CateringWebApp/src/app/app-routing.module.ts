import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FoodsModule} from './components/foods/foods.module'
import { MenusModule } from './components/menus/menus.module';
// import { LoginModule } from './components/login/login.module';
import { MenusComponent } from './components/menus/menus.component';
import { ProductsModule } from './components/products/products.module';
import { ProductsComponent } from './components/products/products.component';
import { TableOfFoodsComponent } from './components/foods/table-of-foods/table-of-foods.component';
import { CalendarModule } from './components/calendar/calendar.module';
import { CalendarComponent } from './components/calendar/calendar.component';
import { OrderComponent } from './components/order/order.component';
import { EventTypeComponent } from './components/event-type/event-type.component';
import { MenuSelectComponent } from './components/menus/menu-select/menu-select.component';
import { ManagerLoginComponent } from './manager-login/manager-login.component';
import { MenuCComponent } from './components/Customer/menu-c/menu-c.component';
import { MakeAnOrderComponent } from './components/Customer/make-an-order/make-an-order.component';
import { AboutUsComponent } from './components/Site/about-us/about-us.component';
import { MyOrderComponent } from './components/Customer/my-order/my-order.component';
import { TheDishRecipeComponent } from './components/Manager/the-dish-recipe/the-dish-recipe.component';
import { DemandForGraphFoodComponent } from './components/Manager/demand-for-graph-food/demand-for-graph-food.component';
import { MoreDetailsOrderComponent } from './components/Manager/more-details-order/more-details-order.component';
import { OrdersAwaitingConfirmationComponent } from './components/Manager/orders-awaiting-confirmation/orders-awaiting-confirmation.component';
import { HomeComponent } from './components/Site/home/home.component';
import { NavComponent } from './components/Site/nav/nav.component';
import { NewMenueComponent } from './components/menus/new-menue/new-menue.component';
import { MenusListByDishComponent } from './components/Manager/menus-list-by-dish/menus-list-by-dish.component';
import { ShoppingListComponent } from './components/Manager/shopping-list/shopping-list.component';

// const routes: Routes = [];

const routes: Routes = [
  // { path: "", redirectTo: "/login", pathMatch: "full" },
  {path:"login",component:ManagerLoginComponent},
  {path:"menus",component:MenusComponent},
  {path:"products",component:ProductsComponent},
  {path:"foods",component:TableOfFoodsComponent},
  {path:"calendar",component:CalendarComponent},
  {path:"order",component:OrderComponent},
  {path:"eventType",component:EventTypeComponent},
  {path: "*", component: CalendarComponent },
  {path: "MenuSelect", component: MenuSelectComponent },
  {path: "MenuC", component: MenuCComponent },
  {path: "MenuC/MakeAnOrder", component: MakeAnOrderComponent },
  {path: "AboutUs", component: AboutUsComponent },
  {path: "MyOrder", component: MyOrderComponent },
  {path: "TheDishRecipe", component: TheDishRecipeComponent },
  {path: "DemandForGraphFood", component: DemandForGraphFoodComponent },
  {path: "MoreDetailsOrder", component: MoreDetailsOrderComponent },
  {path: "OrdersAwaitingConfirmation", component: OrdersAwaitingConfirmationComponent },
  {path: "home", component: HomeComponent },
  {path: "myOrders", component: OrderComponent },
  {path: "manage", component: NavComponent,children:[
    {path:"addOrder",component:MakeAnOrderComponent},
    {path:"calendar",component:CalendarComponent},
    {path:"awaitingOrders",component:OrdersAwaitingConfirmationComponent},
    {path: "newMenu/:menuID", component: NewMenueComponent },
    {path: "shoppingList", component: ShoppingListComponent }
  ] },
  {path: "newMenu/:menuID", component: NewMenueComponent },
  {path: "", component: HomeComponent },





];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    FoodsModule,
    // LoginModule,
    MenusModule,
    ProductsModule,
    CalendarModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
