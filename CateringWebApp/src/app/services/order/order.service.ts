import { Injectable } from '@angular/core';
import { Order } from '../../models/order';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http:HttpClient) { }
  getAllOrders()
  {
     return this.http.get<Order[]>(environment.apiURL+"Order/Get");
  }
  getAaitingOrders()
  {
    return this.http.get<Order[]>(environment.apiURL+"Order/GetAaitingOrders");

  }
  getOrdersByID(order
    )
  {
     return this.http.get<Order>(environment.apiURL+"Order/Get"+order.OrderID);
  }
  addOrder(addOrder:Order)
  {
    return this.http.post<any>(environment.apiURL+"Order/addOrder",addOrder);
  }
  deleteOrder(id:number)
  {
    return this.http.delete(environment.apiURL+"Order/DeleteOrder/"+id);
  }
  updateOrder(id:number,newOrder:Order)
  {
    return this.http.put<Order>(environment.apiURL+"Order/updateOrder/"+id,newOrder);
  }
}

