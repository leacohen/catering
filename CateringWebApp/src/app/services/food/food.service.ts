import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { food } from 'src/app/models/food';

@Injectable({
  providedIn: 'root'
})
export class FoodService {

  constructor(private http: HttpClient) { }

  getAllFood()
  {
    debugger;
      return this.http.get<food[]>(environment.apiURL+"Food/GetAll");
  }
  getFoodById(id:number)
  {
     return this.http.get<food>(environment.apiURL+"food/getFoodById/"+id);
  }
  addFood(newfood:food)
  {
     return this.http.post<any>(environment.apiURL+"food/addFood",newfood);
  }
  deleteFood(id:number)
  {
    return this.http.delete(environment.apiURL+"food/Delete/"+id);
  }
  updateFood(id:number,newfood:food)
  {
    return this.http.put<food>(environment.apiURL+"food/updateFood/"+id,newfood);
  }
}
