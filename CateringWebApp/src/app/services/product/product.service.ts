import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { product } from '../../models/product';
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

   //החזרת כל המוצרים
   getAllProducts()
   {
     return this.http.get<product[]>(environment.apiURL+"Product/Get");
   }

   //חיפוש מוצר לפי קוד
   getProductById(id)
   {
     return this.http.get<product>(environment.apiURL+"Product/Get/"+id);
   }

   //הוספה
   addProduct(newProduct:product)
   {
       return this.http.post<any>(environment.apiURL+"Product/AddProduct",newProduct);
   }

   //עידכון מוצר
   updateProduct(id,productName:string)
   {
     return this.http.put<product>(environment.apiURL+"Product/Update/"+id,productName);
   }

   //מחיקת מוצר
   deleteProduct(id:number)
   {
    return this.http.delete<any>(environment.apiURL+"Product/Delete/"+id);
  }
}
