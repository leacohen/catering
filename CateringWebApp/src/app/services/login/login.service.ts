import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Manager } from '../../models/manager';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }
  managerLogin:Manager;
UserName="שירה ישראלי"
  //החזרת כל המנהלים
  getAll()
  {
    return this.http.get<Manager[]>(environment.apiURL+"Manager/GetAll");
  }
  //חיפוש מהל לפי סיסמא
  searchManager(manager:Manager)
  {
    return this.http.post<any>(environment.apiURL+"Manager/SearchManager",manager);
  }
  //הוספה
  addManager(manager:Manager)
  {
    return this.http.post<any>(environment.apiURL+"Manager/AddManager",manager);
  }
  //עידכון
  updateManager(id:number,upManager:Manager)
  {
    return this.http.put<any>(environment.apiURL+"Manager/UpdateManager/"+id,upManager);
  }
  //מחיקה
  deleteManager(id:number)
  {
    return this.http.delete<any>(environment.apiURL+"Manager/DeleteManager/"+id);
  }
}

