import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment} from 'src/environments/environment';
import { EventType } from '../../models/eventType';

@Injectable({
  providedIn: 'root'
})
export class EventTypeService {

  constructor(private http:HttpClient) { }

  getAllEventType()
  {
     return this.http.get<EventType[]>(environment.apiURL+"eventTypes/GetAll");
  }
  addEventType(newEventType:EventType)
  {
     return this.http.post<any>(environment.apiURL+"eventTypes/addEventTypes",newEventType);
  }

}
