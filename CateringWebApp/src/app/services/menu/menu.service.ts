import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { menu } from '../../models/menu';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private http: HttpClient) { }

  //החזרת כל התפריטים
  getAllMenus()
  {
    return this.http.get<menu[]>(environment.apiURL+"Menu/GetAllMenus");
  }

  //חיפוש תפריט לפי קוד
  getMenuById(id)
  {
    return this.http.get<menu>(environment.apiURL+"Menu/GetMenu"+id);
  }

  //הוספת תפריט
 addMenu(newMenu:menu)
  {
   return this.http.post<any>(environment.apiURL+"Menu/AddMenu",newMenu);
  }

  //עידכון תפריט
  updateMenu(id,upMenu:menu)
  {
    return this.http.put<menu>(environment.apiURL+"Menu/UpdateMenu"+id,upMenu);
  }

  //מחיקת תפריט
  deleteMenu(id:number)
  {
    return this.http.delete<any>(environment.apiURL+"Menu/DeleteMenu"+id);
  }

}
