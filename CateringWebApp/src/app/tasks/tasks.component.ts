import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CalendarOptions } from '@fullcalendar/angular';
import { TheDishRecipeComponent } from '../components/Manager/the-dish-recipe/the-dish-recipe.component';
import { db } from '../models/db';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {


  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    // dateClick: this.handleDateClick.bind(this), // bind is important!
    events: [],
    eventClick:this.handleEventClick.bind(this)
  };
  toDoList = new Array<{foodID, foodName, date:Date, numDays, amount }>();

  events = new Array<{ title, date,foodID ,foodName}>();
  constructor(public dialog: MatDialog,private DB: db) {

    this.setTaskList();
    this.calendarOptions.events = this.events;
    this.ngOnInit()
  }

  ngOnInit(): void {
  }

  setTaskList() {
    // let helpDate = new Date();
    // helpDate.setDate(helpDate.getDate() + 7)
    // let helper = this.DB.orderDb.filter(o => o.EventDate >= new Date() && o.EventDate < helpDate);

    this.DB.orderDb.forEach(o => {
      o.OrderDetails = this.DB.orderDetailsDB.filter(od => od.OrderID == o.OrderID);
      o.OrderDetails.forEach(od => {
        od.FoodInMealMenu = this.DB.foodInMealMenuDB.find(f => f.FoodInMealMenuID == od.FoodInMealMenuID);
        od.FoodInMealMenu.Foods = this.DB.foodDB.find(f => f.FoodID == od.FoodInMealMenu.FoodID);
        this.toDoList.push(
          {
            foodID:od.FoodInMealMenu.FoodID,
            foodName: od.FoodInMealMenu.Foods.FoodName,
            date: o.EventDate,
            amount: o.MealsAmount,
            numDays: od.FoodInMealMenu.Foods.NumDaysToMakeBefore
          }
        )
      })
    })
    debugger;
    this.toDoList.forEach(todo => {
      let helpDate = new Date(todo.date);
      helpDate.setDate(helpDate.getDate() - todo.numDays)
      this.events.push({foodID:todo.foodID , foodName:todo.foodName, title: todo.amount + ' ' + todo.foodName + ' לתאריך ' + todo.date.getDate()+'/'+todo.date.getMonth(), date: helpDate })
    })
  }

  handleEventClick(arg)
  {
    this.openDialog(arg.event._def.extendedProps.foodID,arg.event._def.extendedProps.foodName);
   
  }

  openDialog(id,name) {
    this.dialog.open(TheDishRecipeComponent,
      { width: '500px',
      data: {
        id: id,
        name:name,
        // img:element.FoodPicture
      }
      });
  }
}
