﻿using DAL;
using DTO;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
   public class FoodService
    {
        public static void SendToAddFood(FoodModel food)
        {
            FoodsTBL foodTBL = FoodModel.ConvertToFoodTBl(food);
            FoodsTBL.AddFood(foodTBL);
        }

        public static List<FoodModel> SendToGetAllFoods()
        {
            List<FoodModel> foodsList = new List<FoodModel>();
            foreach (var food in FoodsTBL.GetAllFoods())
            {
               foodsList.Add(FoodModel.ConvertToFoodModel(food));
            }
           // SendEmail.SendMail("שליחת מייל מהפרויקט שלנוווו", "האם זה הצליח?", "l0548432540@gmail.com");
            return foodsList;
        }

        //החזרת רשימת התפריטים בהם המאכל כלול
        public static List<MenusModel> GetMeunsListForFood(int foodTd)
        {

            return MenusModel.ConvertListToMenusModel(FoodsTBL.GetMenusList(foodTd));
        }

        //מחזירה את מתכון המאכל
        public static List<string> GetRecipe(int foodID)
        {
            return  FoodsTBL.GetRecipe(foodID);
        }
    }
}
