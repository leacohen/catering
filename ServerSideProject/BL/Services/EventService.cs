﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    public class EventService
    {
        //החזרת הכל
        public static List<EventTypesModel> GetAll()
        {
            return EventTypesModel.ConvertToEventTypesModel(EventTypesTBL.GetAll());
        }
    }
}
