﻿using DAL;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.services
{
    public class MenuService
    {

        //החזרת רשימת התפריטים
        public static List<MenusModel> GetMenus()
        {
            return MenusModel.ConvertListToMenusModel(MenusTBL.GetAllMenus());
        }

        //חיפוש תפריט לפי קוד
        public static MenusModel SendToSearchById(int id)
        {
            MenusTBL v = MenusTBL.SearchById(id);
            return MenusModel.ConvertToMenusModel(v);
        }

        //הוספת תפריט
        public static void SendToAddMenu(MenusModel menu)
        {
            MenusTBL m = MenusModel.ConvertToMenusTBL(menu);
            MenusTBL.AddMenu(m);
        }

        //עידכון תפריט
        public static void UpdateMenu(int id, MenusModel menu)
        {
            MenusTBL.UpdateMenu(id, MenusModel.ConvertToMenusTBL(menu));
        }

        //מחיקה
        public static void DeleteMenu(int id)
        {
            MenusTBL.DeleteMenu(id);
        }
    }
}
