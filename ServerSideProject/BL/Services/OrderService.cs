﻿using DAL;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public  class OrderService
    {
        public static List<OrderModel> GetAllOrders()
        {
            return OrderModel.ConvertToOrderModel(OrdersTBL.GetAllOrders());
        }
        public static List<OrderModel> GetAaitingOrders()
        {
            return OrderModel.ConvertToOrderModel(OrdersTBL.GetAllOrders());
        }


        public static OrderModel FindOrder(int id)
        {
           return OrderModel.ConvertToOrderModel(OrdersTBL.FindOrder(id));
        }

        public static void AddOrder(OrderModel order)
        {
            OrdersTBL.AddOrder(OrderModel.ConvertToOrderTBL(order));
        }
    }
}
