﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    class SendEmail
    {

        public static void SendMail(string subjecct, string body, string Address, string from = null, string PathToFile = null)
        {

            // שם המייל- ההזדהות
            MailMessage msg = new MailMessage() { From = new MailAddress("xxxx@gmail.com", "קייטרינג בקלי קלות") };
            if (from != null)
            {
                //למי להחזיר את המייל(ממש לא חובה)
                msg.ReplyToList.Add(from);
            }
            //כתובת
            msg.To.Add(Address);
            //נושא
            msg.Subject = subjecct;
            //גוף התוכן
            msg.Body = body;
            //האם הצורה היא HTML כלומר DIV וכו או סתם טקסט
            msg.IsBodyHtml = true;

            //msg.Priority = MailPriority.High;
            //צירוף קובץ  למייל
            if (PathToFile != null)
            {
                try
                {
                    Attachment attach;
                    attach = new Attachment(PathToFile, "application/pdf");
                    msg.Attachments.Add(attach);
                }
                catch (Exception e)
                {

                    throw e;
                }

            }
            try
            {
                using (SmtpClient client = new SmtpClient())
                {
                    client.EnableSsl = true;
                    client.UseDefaultCredentials = false;
                  //  SmtpServer.Credentials = System.Net.NetworkCredential("שם משתמש", "סיסמה")
                    client.Credentials = new NetworkCredential("xxxxxx@gmail.com", "xxxxxxx");
                    client.Host = "smtp.gmail.com";
                    client.Port = 587;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.Send(msg);
                }
                if (PathToFile != null)
                {
                    msg.Attachments.Dispose();

                }
            }
            catch (Exception e)
            {
                throw e;

            }
        }
       
       



    }
}
