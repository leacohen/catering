﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;

namespace BL.services
{
    public class productInFoodService
    {

          //פונקצית שליחה להוספת מתכון
          public static void sendToAddRecipe(ProductInFoodModel recipe)
          {
            ProductInFoodTBL.AddRecipe(ProductInFoodModel.ConvertToProductInFoodTBL(recipe));
          }
    }
}
