﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
   public class ProductService
    {

        //החזרה לפי קוד
        public static ProductModel GetByID(int id)
        {
            return ProductModel.ConvertToProductModel(ProductsTBL.GetProductByID(id));
        }
        //החזרת הכל
        public static List<ProductModel> GetAll()
        {
            return ProductModel.ConvertToProductModel(ProductsTBL.GetAll());
        }
        //הוספת מוצר
        public static void AddProduct(ProductModel productModel)
        {
            ProductsTBL.AddProduct(ProductModel.ConvertToProductTBL(productModel));
        }
        //עידכון
        public static void UpdateProduct(int id, string productName)
        {
            ProductsTBL.UpdateProduct(id, productName);
        }

       

        //מחיקה
        public static void DeleteProduct(int id)
        {
            ProductsTBL.DeleteProduct(id);
        }

        
    }
}
