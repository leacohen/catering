﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    public  class ManagerService
    {

        //החזרת הכל
        public static List<ManagersModel> GetAll()
        {
            return ManagersModel.ConvertToManagerModel(ManagersTBL.GetAll());
        }

        //חיפוש לפי סיסמא
        public static ManagersModel SearchByPassword(string firstName, string lastName, string password)
        {
            return ManagersModel.ConvertToManagerModel(ManagersTBL.SearchByPassword(firstName,lastName,password));
        }

        //הוספה
        public static void AddManager(ManagersModel manager)
        {
            ManagersTBL managerTBL = ManagersModel.ConvertToManagerTBL(manager);
            ManagersTBL.AddManager(managerTBL);
        }

        //עידכון- עריכה
        public static void UpdateManager(int id, ManagersModel manager)
        {
            ManagersTBL.UpdateManager(id, ManagersModel.ConvertToManagerTBL(manager));
        }

        //מחיקה
        public static void RemoveManager(int id)
        {
            ManagersTBL.RemoveManager(id);
        }
    }
}
