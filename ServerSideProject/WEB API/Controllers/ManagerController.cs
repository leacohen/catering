﻿using BL.Services;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

    

namespace WEB_API.Controllers
{
    [RoutePrefix("api/Manager")]
    public class ManagerController : ApiController
    {

        //GET: api/Manager
        [Route("GetAll")]
        //החזרת הכל
        public IHttpActionResult GetAll()
        {
            return Ok(ManagerService.GetAll());
        }

        [Route("SearchManager")]
        [HttpPost]
        //חיפוש מנהל לפי סיסמא
        public IHttpActionResult SearchManager([FromBody]ManagersModel manager)
        {
            ManagersModel manager1=ManagerService.SearchByPassword(manager.FirstName,manager.LastName,manager.ManagerPassword);
            if(manager1!=null)
                return Ok(manager1);
            return InternalServerError();
        }
        [Route("AddManager")]
        [HttpPost]
        // POST: api/Manager
        //הוספת מנהל
        public IHttpActionResult AddManager([FromBody]ManagersModel manager)
        {
            try
            {
                ManagerService.AddManager(manager);
                return Ok();
            }
            catch
            {
                return InternalServerError();
            }

        }
        //עידכון מנהל
        [Route("DeleteManager/{id}")]
        [HttpPut]
        // PUT: api/Manager/5
        public IHttpActionResult UpdateManager(int id, [FromBody]ManagersModel manager)
        {
            try
            {
                ManagerService.UpdateManager(id, manager);
                return Ok();
            }
            catch
            {
                return InternalServerError();
            }
        }

        //מחיקת מנהל
        [Route("DeleteManager/{id}")]
        [HttpDelete]
        // DELETE: api/Manager/5
        public IHttpActionResult DeleteManager(int id)
        {
            try {
                ManagerService.RemoveManager(id);
                return Ok();
            }
            catch {
                return NotFound();
            }
        }
    }
}
