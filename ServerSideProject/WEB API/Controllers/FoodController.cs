﻿using System;
using DTO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BL.Services;

namespace WEB_API.Controllers
{
    [RoutePrefix("api/Food")]
    public class FoodController : ApiController
    {
        // GET: api/Food
        
        [Route("GetAll")]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            List<FoodModel> foods= FoodService.SendToGetAllFoods();
            return Ok(foods);
        }
        [Route("GetRecipe/{id}")]
        // GET: api/Food/5
        public IHttpActionResult GetRecipe(int id)
        {
            List<string> recipe = FoodService.GetRecipe(id);
            return Ok(recipe);
        }
        
        [HttpPost]
        [Route("GetMenu")]
        public IHttpActionResult GetMenu(FoodModel food)
        {
           return Ok(FoodService.GetMeunsListForFood(food.FoodID));
        }


        // POST: api/Food
        public IHttpActionResult AddFood([FromBody]FoodModel foodModel)
        {
            try
            {
                FoodService.SendToAddFood(foodModel);
                return Ok();
            }
            catch {
                return InternalServerError();
            }
        }

        // PUT: api/Food/5
        public void UpdateFood(int id, [FromBody]string value)
        {

        }

        // DELETE: api/Food/5
        public void Delete(int id)
        {
        }
    }
}
