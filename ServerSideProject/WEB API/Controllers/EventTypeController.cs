﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BL.Services;
using DTO;

namespace WEB_API.Controllers
{
    [RoutePrefix("api/eventTypes")]
    public class EventTypeController : ApiController
    {


        [Route("GetAll")]
        [HttpGet]
        //החזרת הכל
        // GET: api/eventTypes
        public IHttpActionResult GetAll()
        {
            List<EventTypesModel> eventTypes = EventService.GetAll();
            return Ok(eventTypes);
        }
    }
}