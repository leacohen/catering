﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BL.Services;
using DTO;

namespace WEB_API.Controllers
{
    [RoutePrefix("api/Product")]
    public class ProductController : ApiController
    {
        [Route("Get")]
        [HttpGet]
        //החזרת הכל
        // GET: api/Product
        public IHttpActionResult Get()
        {
            List<ProductModel> Products = ProductService.GetAll();
            return Ok(Products);
        }

        [Route("Get/{id}")]
        [HttpGet]
        //החזרה לפי קוד
        // GET: api/Product/5
        public IHttpActionResult Get(int id)
        {
            try {
               return Ok(ProductService.GetByID(id));
            }
            catch { 
                return NotFound();
            }

        }

        //הוספה
        [Route("AddProduct")]
        [HttpPost]
        // POST: api/Product
        public IHttpActionResult AddProduct([FromBody]ProductModel productModel)
        {
            try {
                ProductService.AddProduct(productModel);
                return Ok();
            }
            catch
            {
                return InternalServerError();
            }
            
        }

        //עידכון
        [Route("Update/{id}")]
        [HttpPut]
        // PUT: api/Product/5
        public IHttpActionResult Update(int id, [FromBody]string productName)
        {
            try {
                ProductService.UpdateProduct(id, productName);
                    return Ok(); 
            }
            catch {
                return NotFound();
            }
        }

        //מחיקה
        [Route("Delete/{id}")]
        [HttpDelete]
        // DELETE: api/Product/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                ProductService.DeleteProduct(id);
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }
    }
}
