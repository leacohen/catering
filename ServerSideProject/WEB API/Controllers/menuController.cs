﻿using BL.services;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    [RoutePrefix("api/menu")]
    public class menuController : ApiController
    {
        // GET: api/menu
        //החזרת רשימת התפריטים
        [Route("GetAllMenus")]
        public IHttpActionResult GetAllMenus()
        {
            return Ok(MenuService.GetMenus());
        }
        [Route("GetMenu{id}")]
        [HttpGet]
        // GET: api/menu/5
        //מחזירה את פרטי התפריט עפ"י קוד
        public IHttpActionResult GetMenu(int id)
        {
            MenusModel menu = MenuService.SendToSearchById(id);
            if (menu != null)
                return Ok(menu);
            return InternalServerError();
        }
        [Route("AddMenu")]
        // POST: api/menu
        //הוספת תפריט
        public IHttpActionResult AddMenu([FromBody]MenusModel menu)
        {
            try
            {
                MenuService.SendToAddMenu(menu);
                return Ok();
            }
            catch
            {
                return InternalServerError();
            }
        }
        [HttpPut]
        [Route("UpdateMenu{id}")]
        // PUT: api/menu/5
        //עידכון תפריט
        public IHttpActionResult UpdateMenu(int id, [FromBody]MenusModel menu)
        {
            try
            {
                MenuService.UpdateMenu(id, menu);
                return Ok();
            }
            catch
            {
                return InternalServerError();
            }
        }
        //מחיקה
        // DELETE: api/menu/5
        [Route("DeleteMenu{id}")]
        public IHttpActionResult DeleteMenu(int id)
        {
            try {
                MenuService.DeleteMenu(id);
                return Ok();
            }
            catch {
                return NotFound();
            }
        }
    }
}
