﻿using BL;
using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WEB_API.Controllers
{
    [RoutePrefix("api/Order")]

    public class OrderController : ApiController
    {
        
        [Route("Get")]
        // GET: api/Order
        //החזרת כל ההזמנות
        public IHttpActionResult Get()
        {
            var x = OrderService.GetAllOrders();
            return Ok(x);
        }

        [Route("GetAaitingOrders")]
        // GET: api/Order
        //החזרת ההזמנות שמחכות לאישור
        public IHttpActionResult GetAaitingOrders()
        {
            var x = OrderService.GetAaitingOrders();
            return Ok(x);
        }


        [Route("Get/{id}")]
        // GET: api/Order/5
        //החזרת הזמנה עפ"י קוד
        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(OrderService.FindOrder(id));
            }
            catch
            {
                return NotFound();
            }
        }



        [Route("AddOrder")]
        //הוספת הזמנה חדשה
        // POST: api/Order
        public IHttpActionResult AddOrder([FromBody]OrderModel orderModel)
        {
            try
            {
                orderModel.EventTime = DateTime.Today;
                orderModel.OrderDate = DateTime.Today;
                OrderService.AddOrder(orderModel);
                return Ok();
            }
            catch(Exception ex)
            {
                return InternalServerError();
            }
        }

        // PUT: api/Order/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Order/5
        public void Delete(int id)
        {
        }
    }
}
