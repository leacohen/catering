﻿using BL.services;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    [RoutePrefix("api/productInFood")]
    public class productInFoodController : ApiController
    {
        // GET: api/productInFood
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/productInFood/5
        public string Get(int id)
        {
            return "value";
        }
        [Route("addRecipe")]
        // POST: api/productInFood
        public void Post([FromBody]string value)
        {


        }
        //הוספת מתכון
        [Route("addRecipe")]
        public IHttpActionResult addRecipe([FromBody]ProductInFoodModel productInFood)
        {
            try
            {
                productInFoodService.sendToAddRecipe(productInFood);
                return Ok();
            }
            catch
            {
                return InternalServerError();
            }

        }
        // PUT: api/productInFood/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/productInFood/5
        public void Delete(int id)
        {
        }
    }
}
