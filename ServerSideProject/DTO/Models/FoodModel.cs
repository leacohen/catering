﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class FoodModel
   {
        public static FoodModel ConvertToFoodModel(FoodsTBL foodTBL)
        {
            FoodModel foodModel = new FoodModel();
            foodModel.FoodID = foodTBL.FoodID;
            foodModel.FoodName = foodTBL.FoodName;
            foodModel.FoodPicture = foodTBL.FoodPicture;
            foodModel.AbleToFreeze = foodTBL.AbleToFreeze.GetValueOrDefault();
            foodModel.NumDaysToMakeBefore = foodTBL.NumDaysToMakeBefore.GetValueOrDefault();
            return foodModel;
        }

        public static FoodsTBL ConvertToFoodTBl(FoodModel foodModel)
        {
            FoodsTBL foodTBL = new FoodsTBL();
            foodTBL.FoodID = foodModel.FoodID;
            foodTBL.FoodName = foodModel.FoodName;
            foodTBL.FoodPicture = foodModel.FoodPicture;
            foodTBL.AbleToFreeze = foodModel.AbleToFreeze;
            foodTBL.NumDaysToMakeBefore = foodModel.NumDaysToMakeBefore;
            return foodTBL;
        }
        public int FoodID { get; set; }
        public string FoodName { get; set; }
        public string FoodPicture { get; set; }
        public  bool AbleToFreeze { get; set; }
        public int NumDaysToMakeBefore { get; set; }
    }
}
