﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    class OrderAdditionsModel
    {
        public int OrderAddition { get; set; }
        public int OrderID { get; set; }
        public int AdditionID { get; set; }
        //המרה למודל
        public static OrderAdditionsModel ConvertToOrderAdditionsModel(OrderAdditionsTBL orderAdditionsTBL)
        {
            OrderAdditionsModel orderAdditionsModel = new OrderAdditionsModel();
            orderAdditionsModel.OrderAddition = orderAdditionsTBL.OrderAddition;
            orderAdditionsModel.OrderID = orderAdditionsTBL.OrderID.GetValueOrDefault();
            orderAdditionsModel.AdditionID = orderAdditionsTBL.AdditionID.GetValueOrDefault();
            return orderAdditionsModel;
        }
        //DAL המרה ל 
        public static OrderAdditionsTBL ConvertToOrderAdditionsTBL(OrderAdditionsModel orderAdditionsModel)
        {
            OrderAdditionsTBL orderAdditionsTBL = new OrderAdditionsTBL();
            orderAdditionsTBL.OrderAddition = orderAdditionsModel.OrderAddition;
            orderAdditionsTBL.OrderID = orderAdditionsModel.OrderID;
            orderAdditionsTBL.AdditionID = orderAdditionsModel.AdditionID;
            return orderAdditionsTBL;
        }

        //המרת רשימה למודל
        public static List<OrderAdditionsModel> ConvertToOrderAdditionsModel(List<OrderAdditionsTBL> orderAdditionsTBL)
        {
            List<OrderAdditionsModel> orderAdditionsModel = new List<OrderAdditionsModel>();
            foreach (var item in orderAdditionsTBL)
            orderAdditionsModel.Add(OrderAdditionsModel.ConvertToOrderAdditionsModel(item));
            return orderAdditionsModel;
        }

        //DAL המרת רשימה ל 
        public static List<OrderAdditionsTBL> ConvertToOrderAdditionsTBL(List<OrderAdditionsModel> orderAdditionsModel)
        {
            List<OrderAdditionsTBL> orderAdditionsTBL = new List<OrderAdditionsTBL>();
            foreach (var item in orderAdditionsModel)
                orderAdditionsTBL.Add(OrderAdditionsModel.ConvertToOrderAdditionsTBL(item));
            return orderAdditionsTBL;
        }

    }
}
