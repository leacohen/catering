﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    class FoodInMealMenuModel
    {
        public int FoodInMealMenuID { get; set; }
        public int MealMenuId { get; set; }
        public int FoodId { get; set; }

        public static FoodInMealMenuModel ConvertToFoodInMealMenuModel(FoodInMealMenuTBL foodInMealMenuTBL)
        {
            FoodInMealMenuModel foodInMealMenuModel = new FoodInMealMenuModel();
            foodInMealMenuModel.FoodInMealMenuID = foodInMealMenuTBL.FoodInMealMenuID;
            foodInMealMenuModel.MealMenuId = foodInMealMenuTBL.MealMenuId.GetValueOrDefault();
            foodInMealMenuModel.FoodId = foodInMealMenuTBL.FoodId.GetValueOrDefault();
            return foodInMealMenuModel;
        }

        public static FoodInMealMenuTBL ConvertToFoodInMealMenuTBL(FoodInMealMenuModel foodInMealMenuModel)
        {
            FoodInMealMenuTBL foodInMealMenuTBL = new FoodInMealMenuTBL();
            foodInMealMenuTBL.FoodInMealMenuID = foodInMealMenuModel.FoodInMealMenuID;
            foodInMealMenuTBL.MealMenuId = foodInMealMenuModel.MealMenuId;
            foodInMealMenuTBL.FoodId = foodInMealMenuModel.FoodId;
            return foodInMealMenuTBL;
        }
    }
}
