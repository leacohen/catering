﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class MenusModel
    {
        public int MenuID { get; set; }
        public string MenuName { get; set; }
        public int MinMealNum { get; set; }
        public int MealPrice { get; set; }

        public static MenusModel ConvertToMenusModel(MenusTBL menusTBL)
        {
            MenusModel menusModel = new MenusModel();
            menusModel.MenuID = menusTBL.MenuID;
            menusModel.MenuName = menusTBL.MenuName;
            menusModel.MinMealNum = menusTBL.MinMealNum.GetValueOrDefault();
            menusModel.MealPrice = menusTBL.MealPrice.GetValueOrDefault();
            return menusModel;
        }

        public static MenusTBL ConvertToMenusTBL(MenusModel menusModel)
        {
            MenusTBL menusTBL = new MenusTBL();
            menusTBL.MenuID = menusModel.MenuID;
            menusTBL.MenuName = menusModel.MenuName;
            menusTBL.MinMealNum = menusModel.MinMealNum;
            menusTBL.MealPrice = menusModel.MealPrice;
            return menusTBL;
        }
        public static List<MenusModel> ConvertListToMenusModel(List<MenusTBL> ListMenusTBL)
        {
            List<MenusModel> listMenus = new List<MenusModel>();
            foreach (var menu in ListMenusTBL)
            {
                listMenus.Add(ConvertToMenusModel(menu));
            }
            return listMenus;
        }
        public static List<MenusTBL> ConvertListToMenusTBL(List<MenusModel> ListMenusModel)
        {
            List<MenusTBL> listMenus = new List<MenusTBL>();
            foreach (var menu in ListMenusModel)
            {
                listMenus.Add(ConvertToMenusTBL(menu));
            }
            return listMenus;
        }

    }
}
