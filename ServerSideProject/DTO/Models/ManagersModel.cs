﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ManagersModel
    {
        public int MangerID { get; set; }
        public string ManagerPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ManagerEmail { get; set; }


        public static ManagersModel ConvertToManagerModel(ManagersTBL managerTBL)
        {
            ManagersModel managerModel = new ManagersModel();
            managerModel.MangerID = managerTBL.MangerID;
            managerModel.ManagerPassword = managerTBL.ManagerPassword;
            managerModel.FirstName = managerTBL.FirstName;
            managerModel.LastName = managerTBL.LastName;
            managerModel.ManagerEmail = managerTBL.ManagerEmail;
            return managerModel;
        }

        public static ManagersTBL ConvertToManagerTBL(ManagersModel managerModel)
        {
  
            ManagersTBL managerTBL = new ManagersTBL();
            managerTBL.MangerID = managerModel.MangerID;
            managerTBL.ManagerPassword = managerModel.ManagerPassword;
            managerTBL.FirstName = managerModel.FirstName;
            managerTBL.LastName = managerModel.LastName;
            managerTBL.ManagerEmail = managerModel.ManagerEmail;
            return managerTBL;
        }



        public static List<ManagersModel> ConvertToManagerModel(List<ManagersTBL> managersTBL)
        {
            List<ManagersModel> managersModel = new List<ManagersModel>();
            foreach (var item in managersTBL)
            {
                managersModel.Add(ConvertToManagerModel(item));
            }
            return managersModel;
        }


        public static List<ManagersTBL> ConvertToManagerTBL(List<ManagersModel> managersModel)
        {
            List<ManagersTBL> managersTBL = new List<ManagersTBL>();
            foreach (var item in managersModel)
            {
                managersTBL.Add(ConvertToManagerTBL(item));
            }
            return managersTBL;
        }

    }
}
