﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace DTO
{
    public class EventTypesModel
    {
        public int EventID { get; set; }
        public string EventName { get; set; }

        public static EventTypesModel ConvertToEventTypesModel(EventTypesTBL eventTypesTBL)
        {

            EventTypesModel eventTypesModel = new EventTypesModel()
            {
                EventID = eventTypesTBL.EventID,
                EventName = eventTypesTBL.EventName

            };
            return eventTypesModel;
        }

        //המרה לDAL
        public static EventTypesTBL ConvertToEventTypesTBL(EventTypesModel eventTypesModel)
        {
            EventTypesTBL eventTypesTBL = new EventTypesTBL()
            {
                EventID = eventTypesModel.EventID,
                EventName = eventTypesModel.EventName
            };
            return eventTypesTBL;
        }
        //המרת רשימה למודל
        public static List<EventTypesModel> ConvertToEventTypesModel(List<EventTypesTBL> listEventTypesTBL)
        {
            List<EventTypesModel> listEventTypesModels = new List<EventTypesModel>();
            foreach (var item in listEventTypesTBL)
            {
                listEventTypesModels.Add(EventTypesModel.ConvertToEventTypesModel(item));
            }
            return listEventTypesModels;
        }
        //DAL המרת רשימה ל
        public static List<EventTypesTBL> ConvertToEventTypesTBL(List<EventTypesModel> listEventTypesModel)
        {
            List<EventTypesTBL> listEventTypesTBL = new List<EventTypesTBL>();
            foreach (var item in listEventTypesModel)
            {
                listEventTypesTBL.Add(EventTypesModel.ConvertToEventTypesTBL(item));
            }
            return listEventTypesTBL;
        }
    }
}
