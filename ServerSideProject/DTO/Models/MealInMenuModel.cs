﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    class MealInMenuModel
    {
        public int MealMenuId { get; set; }
        public int MenuID { get; set; }
        public int MealID { get; set; }
        public int OrdinalNumber { get; set; }
        public int AlternativeAmountToChoose { get; set; }

        public static MealInMenuModel ConvertToMealInMenuModel(MealInMenuTBL mealInMenuTBL)
        {
            MealInMenuModel mealInMenuModel = new MealInMenuModel();
            mealInMenuModel.MealMenuId = mealInMenuTBL.MealMenuId;
            mealInMenuModel.MenuID = mealInMenuTBL.MenuID.GetValueOrDefault();
            mealInMenuModel.MealID = mealInMenuTBL.MealID.GetValueOrDefault();
            mealInMenuModel.OrdinalNumber = mealInMenuTBL.OrdinalNumber.GetValueOrDefault();
            mealInMenuModel.AlternativeAmountToChoose = mealInMenuTBL.AlternativeAmountToChoose.GetValueOrDefault();
            return mealInMenuModel;
        }

        public static MealInMenuTBL ConvertToMealInMenuTBL(MealInMenuModel mealInMenuModel)
        {
            MealInMenuTBL mealInMenuTBL = new MealInMenuTBL();
            mealInMenuTBL.MealMenuId = mealInMenuModel.MealMenuId;
            mealInMenuTBL.MenuID = mealInMenuModel.MenuID;
            mealInMenuTBL.MealID = mealInMenuModel.MealID;
            mealInMenuTBL.OrdinalNumber = mealInMenuModel.OrdinalNumber;
            mealInMenuTBL.AlternativeAmountToChoose = mealInMenuModel.AlternativeAmountToChoose;
            return mealInMenuTBL;
        }
    }
}
