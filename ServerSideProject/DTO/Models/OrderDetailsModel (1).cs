﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    class OrderDetailsModel
    {
        public int OrderDetailsID { get; set; }
        public int OrderID { get; set; }
        public int FoodInMealMenuID { get; set; }
        public int Amount { get; set; }
        //MODEL המרה ל 
        public static OrderDetailsModel ConvertToOrderDetailsModel(OrderDetailsTBL orderDetailsTBL)
        {
            OrderDetailsModel orderDetailsModel = new OrderDetailsModel()
            {
                OrderDetailsID = orderDetailsTBL.OrderDetailsID,
                OrderID = orderDetailsTBL.OrderID.GetValueOrDefault(),
                FoodInMealMenuID = orderDetailsTBL.FoodInMealMenuID.GetValueOrDefault(),
                Amount = orderDetailsTBL.Amount.GetValueOrDefault()
            };
            return orderDetailsModel;
        }

        //DAL המרה ל
        public static OrderDetailsTBL ConvertToOrderDetailsTBL(OrderDetailsModel orderDetailsModel)
        {
            OrderDetailsTBL orderDetailsTBL = new OrderDetailsTBL()
            {
               OrderDetailsID=orderDetailsModel.OrderDetailsID,
               OrderID=orderDetailsModel.OrderID,
               FoodInMealMenuID=orderDetailsModel.FoodInMealMenuID,
               Amount=orderDetailsModel.Amount
            };
            return orderDetailsTBL;
        }

        //המת רשימה למודל
        public static List<OrderDetailsModel> ConvertToOrderDetailsModel(List<OrderDetailsTBL> orderDetailsTBL)
        {
            List<OrderDetailsModel> orderDetailsModel = new List<OrderDetailsModel>();
            foreach (var item in orderDetailsTBL)
                orderDetailsModel.Add(OrderDetailsModel.ConvertToOrderDetailsModel(item));
            return orderDetailsModel;
        }

        //DAL המרת רשימה ל
        public static List<OrderDetailsTBL> ConvertToOrderDetailsTBL(List<OrderDetailsModel> orderDetailsModel)
        {
            List<OrderDetailsTBL> orderDetailsTBL = new List<OrderDetailsTBL>();
            foreach (var item in orderDetailsModel)
                orderDetailsTBL.Add(OrderDetailsModel.ConvertToOrderDetailsTBL(item));
            return orderDetailsTBL;
        }
    }
}
