﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ProductModel
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        //המרת מוצר יחיד למודל
        public static ProductModel ConvertToProductModel(ProductsTBL productTBL)
        {
            ProductModel productModel = new ProductModel()
            {
                ProductID = productTBL.ProductID,
                ProductName = productTBL.ProductName
            };
            return productModel;
        }
        //DALהמרת מוצר יחיד ל
        public static ProductsTBL ConvertToProductTBL(ProductModel productModel)
        {
            ProductsTBL productsTBL = new ProductsTBL()
            {
                ProductID = productModel.ProductID,
                ProductName = productModel.ProductName
            };
            return productsTBL;
        }

        //המרת רשימה למודל
        public static List<ProductModel> ConvertToProductModel(List<ProductsTBL> listProductsTBL)
        {
            List<ProductModel> listProductModels = new List<ProductModel>();
            foreach (var item in listProductsTBL)
            {
                listProductModels.Add(ProductModel.ConvertToProductModel(item));
            }
            return listProductModels;
        }
        //DAL המרת רשימה של מוצרים ל
        public static List<ProductsTBL> ConvertToProductTBL(List<ProductModel> listProductsModel)
        {
            List<ProductsTBL> listProductTBL = new List<ProductsTBL>();
            foreach (var item in listProductsModel)
            {
                listProductTBL.Add(ProductModel.ConvertToProductTBL(item));
            }
            return listProductTBL;
        }
    }
}
