﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    class AdditionsModel
    {
        public int AdditionID { get; set; }
        public string AdditionName { get; set; }
        public int PriceTo50Meals { get; set; }

      
        //המרה לMODEL
        public static AdditionsModel ConvertToAdditionsModel(AdditionsTBL additionsTBL)
        {

            AdditionsModel additionsModel = new AdditionsModel()
            {
               AdditionID= additionsTBL.AdditionID,
               AdditionName=additionsTBL.AdditionName,
               PriceTo50Meals= additionsTBL.PriceTo50Meals.GetValueOrDefault()

            };
            return additionsModel;
        }

        //המרה לDAL
        public static AdditionsTBL ConvertToAdditionsTBL(AdditionsModel additionsModel)
        {
            AdditionsTBL additionsTBL = new AdditionsTBL()
            {
               AdditionID=additionsModel.AdditionID,
               AdditionName=additionsModel.AdditionName,
               PriceTo50Meals=additionsModel.PriceTo50Meals
            };
            return additionsTBL;
        }


    }
}
