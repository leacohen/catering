﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Models
{
    public class OrderModel
    {
        public int OrderID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string CustomerEmail { get; set; }
        public System.DateTime OrderDate { get; set; }
        public int EventID { get; set; }
        public System.DateTime EventDate { get; set; }
        public System.DateTime EventTime { get; set; }
        public string EventAddress { get; set; }
        public int MealsAmount { get; set; }
        public int MenuID { get; set; }
        public bool OkByManager { get; set; }
        public int ClosedPrice { get; set; }
        public bool PaiedBefore { get; set; }
        public bool PaidBalance { get; set; }
        public string Comment { get; set; }
        public int EventTypeID { get; set; }


        //המרה למודל
        public static OrderModel ConvertToOrderModel(OrdersTBL ordersTBL )
        {
            OrderModel orderModel = new OrderModel();
            orderModel.FirstName = ordersTBL.FirstName;
            orderModel.LastName = ordersTBL.LastName;
            orderModel.Phone = ordersTBL.Phone;
            orderModel.CustomerEmail = ordersTBL.CustomerEmail;
            orderModel.OrderDate = ordersTBL.OrderDate.GetValueOrDefault();
            orderModel.EventID = ordersTBL.EventID.GetValueOrDefault();
            orderModel.EventDate = ordersTBL.EventDate.GetValueOrDefault();
            orderModel.EventTime = ordersTBL.EventTime.GetValueOrDefault();
            orderModel.EventAddress = ordersTBL.EventAddress;
            //orderModel.EventTypeID = ordersTBL.EventTypesTBL.EventID;
            orderModel.MealsAmount = ordersTBL.MealsAmount.GetValueOrDefault();
            orderModel.MenuID = ordersTBL.MenuID.GetValueOrDefault();
            orderModel.OkByManager = ordersTBL.OkByManager.GetValueOrDefault();
            orderModel.ClosedPrice = ordersTBL.ClosedPrice.GetValueOrDefault();
            orderModel.PaiedBefore = ordersTBL.PaiedBefore.GetValueOrDefault();
            orderModel.PaidBalance = ordersTBL.PaidBalance.GetValueOrDefault();
            orderModel.Comment = ordersTBL.Comment;
            return orderModel;
        }

        //המרה  ל  DAL          
        public static OrdersTBL ConvertToOrderTBL(OrderModel orderModel)
        {
            OrdersTBL ordersTBL = new OrdersTBL();
            ordersTBL.FirstName = orderModel.FirstName;
            ordersTBL.LastName = orderModel.LastName;
            ordersTBL.Phone = orderModel.Phone;
            ordersTBL.CustomerEmail = orderModel.CustomerEmail;
            ordersTBL.OrderDate = orderModel.OrderDate;
            ordersTBL.EventID = orderModel.EventID;
            ordersTBL.EventDate = orderModel.EventDate;
            ordersTBL.EventTime = orderModel.EventTime;
            ordersTBL.EventAddress = orderModel.EventAddress; 
            ordersTBL.MealsAmount = orderModel.MealsAmount;
            ordersTBL.MenuID = orderModel.MenuID;
            ordersTBL.OkByManager = orderModel.OkByManager;
            ordersTBL.ClosedPrice = orderModel.ClosedPrice;
            ordersTBL.PaiedBefore = orderModel.PaiedBefore;
            ordersTBL.PaidBalance = orderModel.PaidBalance;
            ordersTBL.Comment = orderModel.Comment;
            return ordersTBL;
        }
        //המרת רשימה למודל
        public static List<OrderModel> ConvertToOrderModel(List<OrdersTBL> ordersTBL)
        {
            List<OrderModel> ordersModel = new List<OrderModel>();
            foreach (var order in ordersTBL)
                ordersModel.Add(ConvertToOrderModel(order));
            return ordersModel;
        }

        //המרת רשימה ל DAL
        public static List<OrdersTBL> ConvertToOrderTBL(List<OrderModel> ordersModel)
        {
            List<OrdersTBL> ordersTBL = new List<OrdersTBL>();
            foreach (var order in ordersModel)
                ordersTBL.Add(ConvertToOrderTBL(order));
            return ordersTBL;
        }
    }
}
