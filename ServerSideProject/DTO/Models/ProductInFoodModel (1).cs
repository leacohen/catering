﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class ProductInFoodModel
    {
        public int ProductInFoodID { get; set; }
        public string ProductName { get; set; }
        public int FoodId { get; set; }
        public int ProductID { get; set; }
        public int AmountTo50Meals { get; set; }

        //המרה לMODEL
        public static ProductInFoodModel ConvertToProductInFoodModel(ProductInFoodTBL productInFoodTBL)
        {
            ProductInFoodModel productInFoodModel = new ProductInFoodModel()
            {
                ProductInFoodID = productInFoodTBL.ProductInFoodID,
                FoodId = productInFoodTBL.FoodId.GetValueOrDefault(),
                ProductID = productInFoodTBL.ProductID.GetValueOrDefault(),
                AmountTo50Meals = productInFoodTBL.AmountTo50Meals.GetValueOrDefault()
            };
            return productInFoodModel;
        }

        //המרה לDAL
        public static ProductInFoodTBL ConvertToProductInFoodTBL(ProductInFoodModel productInFoodModel)
        {
            ProductInFoodTBL productInFoodTBL = new ProductInFoodTBL()
            {
              ProductInFoodID = productInFoodModel.ProductInFoodID,
              FoodId = productInFoodModel.FoodId,
              ProductID = productInFoodModel.ProductID,
              AmountTo50Meals = productInFoodModel.AmountTo50Meals
            };
            return productInFoodTBL;
        }

        //המרת רשימה למודל
        public static List<ProductInFoodModel> ConvertToProductInFoodModel(List<ProductInFoodTBL> listProductInFoodTBL)
        {
            List<ProductInFoodModel> listProductInFoodModel = new List<ProductInFoodModel>();
            foreach (var item in listProductInFoodTBL)
            {
                listProductInFoodModel.Add(ProductInFoodModel.ConvertToProductInFoodModel(item));
            }
            return listProductInFoodModel;
        }
        //DAL המרת רשימה ל   
        public static List<ProductInFoodTBL> ConvertToProductInFoodTBL(List<ProductInFoodModel> listProductInFoodModel)
        {
            List<ProductInFoodTBL> listProductInFoodTBL = new List<ProductInFoodTBL>();
            foreach (var item in listProductInFoodModel)
            {
                listProductInFoodTBL.Add(ProductInFoodModel.ConvertToProductInFoodTBL(item));
            }
            return listProductInFoodTBL;
        }
    }
}
