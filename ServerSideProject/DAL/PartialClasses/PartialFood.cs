﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public partial class FoodsTBL
    {
        //הוספת מאכל חדש
        public static void AddFood(FoodsTBL newFood)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                context.FoodsTBL.Add(newFood);
                context.SaveChanges();
            }
        }
        //החזרת כל המאכלים
        public static List<FoodsTBL> GetAllFoods()
        {

            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                var foodList = context.FoodsTBL.ToList();
                return foodList;
            }

        }
        //פונקציה להחזרת כל התפריטים בהם המאכל כלול
        public static List<MenusTBL> GetMenusList(int foodTd)
            {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                //מחזיר רשימת תפריטים בהם כלול המאכל
                var fim= context.FoodInMealMenuTBL.Where(x => x.FoodId ==foodTd);
                IQueryable<MealInMenuTBL> min = fim.Select(fm => fm.MealInMenuTBL);
                List<MenusTBL> mn = min.Select(s => s.MenusTBL).ToList();
                return mn;
            }
        }

        //פונקציה שמחזירה את מתכון המאכל
        public static List<string> GetRecipe(int foodID)
        { 
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                  return  context.ProductInFoodTBL.Where(x => x.FoodId == foodID).Select(x=>x.ProductsTBL.ProductName).ToList();
            }
        }

        //מעדכנת את מתכון המאכל לאחר העריכה
        //public static void UpdateRecipe(int foodID, List<ProductInFoodTBL> productInFoodTBLs)
        //{
        //    using(var context = new ProjectEntities())
        //    {
        //        context.ProductInFoodTBL.Where(x => x.FoodId == foodID);
        //    }
        //}
            
    }
}
