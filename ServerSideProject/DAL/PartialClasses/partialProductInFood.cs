﻿using Microsoft.VisualC.StlClr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public partial class ProductInFoodTBL
    {

        //החזרת מתכון מאכל ע"י קוד
          public static void GetRecipe(int id)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                //var pif = context.ProductInFoodTBL.Where(x=> x.FoodId == id).ToDictionary<ProductsTBL,ProductInFoodTBL>()
                //var p=pif.Select(y=>y.ProductsTBL)


                var pif = context.ProductInFoodTBL.Where(x => x.FoodId == id).Select(y => new { y.ProductsTBL.ProductName, y.AmountTo50Meals }).ToList();

                //return pif;

                // return pif;
            }
        }
       
        //פונקציה להחזרת כל התפריטים בהם המאכל כלול
        public static List<MenusTBL> GetMenusList(int foodTd)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                //מחזיר רשימת תפריטים בהם כלול המאכל
                var fim = context.FoodInMealMenuTBL.Where(x => x.FoodId == foodTd);
                IQueryable<MealInMenuTBL> min = fim.Select(fm => fm.MealInMenuTBL);
                List<MenusTBL> mn = min.Select(s => s.MenusTBL).ToList();
                return mn;
            }
        }

        // הוספת מתכון חדש
        public static void AddRecipe(ProductInFoodTBL recipe)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                context.ProductInFoodTBL.Add(recipe);
                context.SaveChanges();
            }
        }

    }
}
