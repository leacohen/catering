﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public partial class OrdersTBL
    {
        public static List<OrdersTBL> GetAllOrders()
        {
             //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                var orderList = context.OrdersTBL.ToList();
                return orderList;
            }

        }
        public static List<OrdersTBL> GetAaitingOrders()
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                var orderList = context.OrdersTBL.Where(o=>o.OkByManager==false).ToList();
                return orderList;
            }

        }
        public static OrdersTBL FindOrder(int id)
        {
            OrdersTBL order;
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                order = context.OrdersTBL.Where(x => x.OrderID == id).Single();
                return order;
            }
        }

        public static void AddOrder(OrdersTBL order)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                context.OrdersTBL.Add(order);
                context.SaveChanges();
            }
        }
    }
}
