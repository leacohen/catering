﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public partial class ManagersTBL 
    {

        public static List<ManagersTBL> GetAll()
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                return context.ManagersTBL.ToList();
            }
         }

        public static ManagersTBL SearchByPassword(string firstName, string lastName, string password)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
               var manager = context.ManagersTBL.Where(x=>x.ManagerPassword.Equals(password)&&x.FirstName.Equals(firstName)&&x.LastName.Equals(lastName));
                if (manager.Any())
                  return manager.Single();
            }
            return null;
        }

        public static void AddManager(ManagersTBL newManager)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                context.ManagersTBL.Add(newManager);
                context.SaveChanges();
            }
        }

        public static void UpdateManager(int id, ManagersTBL UpManager)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                var mm = context.ManagersTBL.First(m => m.MangerID == id);
                mm.FirstName = UpManager.FirstName;
                mm.LastName = UpManager.LastName;
                mm.ManagerPassword = UpManager.ManagerPassword;
                mm.ManagerEmail = UpManager.ManagerEmail;
                                                            
                context.SaveChanges();
            }

        }
        public static void RemoveManager(int id)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                var manager = context.ManagersTBL.First(m => m.MangerID == id);
                context.ManagersTBL.Remove(manager);
                context.SaveChanges();
            }
        }
    }
}
