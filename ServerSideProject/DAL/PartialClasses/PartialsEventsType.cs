﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public partial class EventTypesTBL
    {
        //החזרת כל סוגי הארועים
        public static List<EventTypesTBL> GetAll()
        {
            using (var context = new ProjectEntities1())
            {
                return context.EventTypesTBL.ToList();
            }
        }

    }
}
