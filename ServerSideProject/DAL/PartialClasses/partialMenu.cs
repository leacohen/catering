﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public partial class MenusTBL
    {
        
        //על פי קוד החזרת תפריט 
        public static MenusTBL SearchById(int id)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                var menu = context.MenusTBL.Where(x => x.MenuID == id);
                if (menu.Any())
                    return menu.Single();
            }
            return null;
        }
        //החזרת כל התפריטים
        public static  List<MenusTBL> GetAllMenus()
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
               var menuList= context.MenusTBL.ToList();
                return menuList;
            }
        }

        //הוספת תפריט
        public static void AddMenu(MenusTBL menus)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                context.MenusTBL.Add(menus);
                context.SaveChanges();
            }
        }

        //עידכון תפריט
        public static void UpdateMenu(int id, MenusTBL updatedMenu)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                MenusTBL menu = context.MenusTBL.First(m => m.MenuID == id);
                menu.MenuName = updatedMenu.MenuName;
                menu.MealPrice = updatedMenu.MealPrice;
                menu.MinMealNum = updatedMenu.MinMealNum;
                
                context.SaveChanges();
            }
        }

        //מחיקת תפריט
        public static void DeleteMenu(int id)
        {  
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                var menu = context.MenusTBL.First(m => m.MenuID == id);
                context.MenusTBL.Remove(menu);
                context.SaveChanges();
            }
        }
    }
}
