﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{ 
    public partial class ProductsTBL
    {

        //החזרת כל המוצרים
        public static List<ProductsTBL> GetAll()
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
               return context.ProductsTBL.ToList();
            }
        }

        //החזרת מוצר לפי קוד
        public static ProductsTBL GetProductByID(int id)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                return context.ProductsTBL.Single(x => x.ProductID == id);
            }
        }

        //הוספת מוצר
        public static void AddProduct(ProductsTBL product)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                context.ProductsTBL.Add(product);
                context.SaveChanges();
            }
        }
       

        //עריכת מוצר
   
        public static void UpdateProduct(int id, string productName)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                var p = context.ProductsTBL.Single(x => x.ProductID == id);
                p.ProductName = productName;
                context.SaveChanges();
            }
        }

        //מחיקת מוצר
        public static void DeleteProduct(int id)
        {
            //using (var context = new ProjectEntities())
            using (var context = new ProjectEntities1())
            {
                var product = context.ProductsTBL.First(p => p.ProductID == id);
                context.ProductsTBL.Remove(product);
                context.SaveChanges();
            }
        }
    }
}
