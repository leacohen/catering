
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/05/2020 21:24:08
-- Generated from EDMX file: C:\Users\1\משולחן העבודה\פרויקט\ServerSideProject\DAL\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Project];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__FoodInMea__FoodI__2F10007B]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FoodInMealMenuTBL] DROP CONSTRAINT [FK__FoodInMea__FoodI__2F10007B];
GO
IF OBJECT_ID(N'[dbo].[FK__FoodInMea__MealM__2E1BDC42]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FoodInMealMenuTBL] DROP CONSTRAINT [FK__FoodInMea__MealM__2E1BDC42];
GO
IF OBJECT_ID(N'[dbo].[FK__MealInMen__MealI__29572725]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MealInMenuTBL] DROP CONSTRAINT [FK__MealInMen__MealI__29572725];
GO
IF OBJECT_ID(N'[dbo].[FK__MealInMen__MenuI__286302EC]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MealInMenuTBL] DROP CONSTRAINT [FK__MealInMen__MenuI__286302EC];
GO
IF OBJECT_ID(N'[dbo].[FK__OrderAddi__Addit__403A8C7D]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrderAdditionsTBL] DROP CONSTRAINT [FK__OrderAddi__Addit__403A8C7D];
GO
IF OBJECT_ID(N'[dbo].[FK__OrderAddi__Order__3F466844]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrderAdditionsTBL] DROP CONSTRAINT [FK__OrderAddi__Order__3F466844];
GO
IF OBJECT_ID(N'[dbo].[FK__OrderDeta__FoodI__440B1D61]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrderDetailsTBL] DROP CONSTRAINT [FK__OrderDeta__FoodI__440B1D61];
GO
IF OBJECT_ID(N'[dbo].[FK__OrderDeta__Order__4316F928]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrderDetailsTBL] DROP CONSTRAINT [FK__OrderDeta__Order__4316F928];
GO
IF OBJECT_ID(N'[dbo].[FK__OrdersTBL__Event__3B75D760]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrdersTBL] DROP CONSTRAINT [FK__OrdersTBL__Event__3B75D760];
GO
IF OBJECT_ID(N'[dbo].[FK__OrdersTBL__MenuI__3C69FB99]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrdersTBL] DROP CONSTRAINT [FK__OrdersTBL__MenuI__3C69FB99];
GO
IF OBJECT_ID(N'[dbo].[FK__ProductIn__FoodI__33D4B598]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ProductInFoodTBL] DROP CONSTRAINT [FK__ProductIn__FoodI__33D4B598];
GO
IF OBJECT_ID(N'[dbo].[FK__ProductIn__Produ__34C8D9D1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ProductInFoodTBL] DROP CONSTRAINT [FK__ProductIn__Produ__34C8D9D1];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[AdditionsTBL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AdditionsTBL];
GO
IF OBJECT_ID(N'[dbo].[EventTypesTBL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EventTypesTBL];
GO
IF OBJECT_ID(N'[dbo].[FoodInMealMenuTBL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FoodInMealMenuTBL];
GO
IF OBJECT_ID(N'[dbo].[FoodsTBL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FoodsTBL];
GO
IF OBJECT_ID(N'[dbo].[ManagersTBL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ManagersTBL];
GO
IF OBJECT_ID(N'[dbo].[MealInMenuTBL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MealInMenuTBL];
GO
IF OBJECT_ID(N'[dbo].[MealsTBL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MealsTBL];
GO
IF OBJECT_ID(N'[dbo].[MenusTBL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MenusTBL];
GO
IF OBJECT_ID(N'[dbo].[OrderAdditionsTBL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrderAdditionsTBL];
GO
IF OBJECT_ID(N'[dbo].[OrderDetailsTBL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrderDetailsTBL];
GO
IF OBJECT_ID(N'[dbo].[OrdersTBL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrdersTBL];
GO
IF OBJECT_ID(N'[dbo].[ProductInFoodTBL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductInFoodTBL];
GO
IF OBJECT_ID(N'[dbo].[ProductsTBL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductsTBL];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'AdditionsTBL'
CREATE TABLE [dbo].[AdditionsTBL] (
    [AdditionID] int IDENTITY(1,1) NOT NULL,
    [AdditionName] varchar(20)  NULL,
    [PriceTo50Meals] int  NULL
);
GO

-- Creating table 'EventTypesTBL'
CREATE TABLE [dbo].[EventTypesTBL] (
    [EventID] int IDENTITY(1,1) NOT NULL,
    [EventName] varchar(20)  NULL
);
GO

-- Creating table 'FoodInMealMenuTBL'
CREATE TABLE [dbo].[FoodInMealMenuTBL] (
    [FoodInMealMenuID] int IDENTITY(1,1) NOT NULL,
    [MealMenuId] int  NULL,
    [FoodId] int  NULL
);
GO

-- Creating table 'FoodsTBL'
CREATE TABLE [dbo].[FoodsTBL] (
    [FoodID] int IDENTITY(1,1) NOT NULL,
    [FoodName] varchar(20)  NULL,
    [FoodPicture] varchar(30)  NULL,
    [AbleToFreeze] bit  NULL,
    [NumDaysToMakeBefore] int  NULL
);
GO

-- Creating table 'ManagersTBL'
CREATE TABLE [dbo].[ManagersTBL] (
    [MangerID] int IDENTITY(1,1) NOT NULL,
    [ManagerPassword] varchar(5)  NOT NULL,
    [FirstName] varchar(15)  NULL,
    [LastName] varchar(10)  NULL,
    [ManagerEmail] varchar(20)  NULL
);
GO

-- Creating table 'MealInMenuTBL'
CREATE TABLE [dbo].[MealInMenuTBL] (
    [MealMenuId] int IDENTITY(1,1) NOT NULL,
    [MenuID] int  NULL,
    [MealID] int  NULL,
    [OrdinalNumber] int  NULL,
    [AlternativeAmountToChoose] int  NULL
);
GO

-- Creating table 'MealsTBL'
CREATE TABLE [dbo].[MealsTBL] (
    [MealID] int IDENTITY(1,1) NOT NULL,
    [MealName] varchar(20)  NULL
);
GO

-- Creating table 'MenusTBL'
CREATE TABLE [dbo].[MenusTBL] (
    [MenuID] int IDENTITY(1,1) NOT NULL,
    [MenuName] varchar(10)  NULL,
    [MinMealNum] int  NULL,
    [MealPrice] int  NULL
);
GO

-- Creating table 'OrderAdditionsTBL'
CREATE TABLE [dbo].[OrderAdditionsTBL] (
    [OrderAddition] int IDENTITY(1,1) NOT NULL,
    [OrderID] int  NULL,
    [AdditionID] int  NULL
);
GO

-- Creating table 'OrderDetailsTBL'
CREATE TABLE [dbo].[OrderDetailsTBL] (
    [OrderDetailsID] int IDENTITY(1,1) NOT NULL,
    [OrderID] int  NULL,
    [FoodInMealMenuID] int  NULL,
    [Amount] int  NULL
);
GO

-- Creating table 'OrdersTBL'
CREATE TABLE [dbo].[OrdersTBL] (
    [OrderID] int IDENTITY(1,1) NOT NULL,
    [FirstName] varchar(15)  NULL,
    [LastName] varchar(10)  NULL,
    [Phone] varchar(9)  NULL,
    [CustomerEmail] varchar(30)  NULL,
    [OrderDate] datetime  NULL,
    [EventID] int  NULL,
    [EventDate] datetime  NULL,
    [EventTime] datetime  NULL,
    [EventAddress] varchar(20)  NULL,
    [MealsAmount] int  NULL,
    [MenuID] int  NULL,
    [OkByManager] bit  NULL,
    [ClosedPrice] int  NULL,
    [PaiedBefore] bit  NULL,
    [PaidBalance] bit  NULL,
    [Comment] varchar(100)  NULL
);
GO

-- Creating table 'ProductInFoodTBL'
CREATE TABLE [dbo].[ProductInFoodTBL] (
    [ProductInFoodID] int IDENTITY(1,1) NOT NULL,
    [FoodId] int  NULL,
    [ProductID] int  NULL,
    [AmountTo50Meals] int  NULL
);
GO

-- Creating table 'ProductsTBL'
CREATE TABLE [dbo].[ProductsTBL] (
    [ProductID] int IDENTITY(1,1) NOT NULL,
    [ProductName] varchar(20)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [AdditionID] in table 'AdditionsTBL'
ALTER TABLE [dbo].[AdditionsTBL]
ADD CONSTRAINT [PK_AdditionsTBL]
    PRIMARY KEY CLUSTERED ([AdditionID] ASC);
GO

-- Creating primary key on [EventID] in table 'EventTypesTBL'
ALTER TABLE [dbo].[EventTypesTBL]
ADD CONSTRAINT [PK_EventTypesTBL]
    PRIMARY KEY CLUSTERED ([EventID] ASC);
GO

-- Creating primary key on [FoodInMealMenuID] in table 'FoodInMealMenuTBL'
ALTER TABLE [dbo].[FoodInMealMenuTBL]
ADD CONSTRAINT [PK_FoodInMealMenuTBL]
    PRIMARY KEY CLUSTERED ([FoodInMealMenuID] ASC);
GO

-- Creating primary key on [FoodID] in table 'FoodsTBL'
ALTER TABLE [dbo].[FoodsTBL]
ADD CONSTRAINT [PK_FoodsTBL]
    PRIMARY KEY CLUSTERED ([FoodID] ASC);
GO

-- Creating primary key on [MangerID] in table 'ManagersTBL'
ALTER TABLE [dbo].[ManagersTBL]
ADD CONSTRAINT [PK_ManagersTBL]
    PRIMARY KEY CLUSTERED ([MangerID] ASC);
GO

-- Creating primary key on [MealMenuId] in table 'MealInMenuTBL'
ALTER TABLE [dbo].[MealInMenuTBL]
ADD CONSTRAINT [PK_MealInMenuTBL]
    PRIMARY KEY CLUSTERED ([MealMenuId] ASC);
GO

-- Creating primary key on [MealID] in table 'MealsTBL'
ALTER TABLE [dbo].[MealsTBL]
ADD CONSTRAINT [PK_MealsTBL]
    PRIMARY KEY CLUSTERED ([MealID] ASC);
GO

-- Creating primary key on [MenuID] in table 'MenusTBL'
ALTER TABLE [dbo].[MenusTBL]
ADD CONSTRAINT [PK_MenusTBL]
    PRIMARY KEY CLUSTERED ([MenuID] ASC);
GO

-- Creating primary key on [OrderAddition] in table 'OrderAdditionsTBL'
ALTER TABLE [dbo].[OrderAdditionsTBL]
ADD CONSTRAINT [PK_OrderAdditionsTBL]
    PRIMARY KEY CLUSTERED ([OrderAddition] ASC);
GO

-- Creating primary key on [OrderDetailsID] in table 'OrderDetailsTBL'
ALTER TABLE [dbo].[OrderDetailsTBL]
ADD CONSTRAINT [PK_OrderDetailsTBL]
    PRIMARY KEY CLUSTERED ([OrderDetailsID] ASC);
GO

-- Creating primary key on [OrderID] in table 'OrdersTBL'
ALTER TABLE [dbo].[OrdersTBL]
ADD CONSTRAINT [PK_OrdersTBL]
    PRIMARY KEY CLUSTERED ([OrderID] ASC);
GO

-- Creating primary key on [ProductInFoodID] in table 'ProductInFoodTBL'
ALTER TABLE [dbo].[ProductInFoodTBL]
ADD CONSTRAINT [PK_ProductInFoodTBL]
    PRIMARY KEY CLUSTERED ([ProductInFoodID] ASC);
GO

-- Creating primary key on [ProductID] in table 'ProductsTBL'
ALTER TABLE [dbo].[ProductsTBL]
ADD CONSTRAINT [PK_ProductsTBL]
    PRIMARY KEY CLUSTERED ([ProductID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [AdditionID] in table 'OrderAdditionsTBL'
ALTER TABLE [dbo].[OrderAdditionsTBL]
ADD CONSTRAINT [FK__OrderAddi__Addit__403A8C7D]
    FOREIGN KEY ([AdditionID])
    REFERENCES [dbo].[AdditionsTBL]
        ([AdditionID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__OrderAddi__Addit__403A8C7D'
CREATE INDEX [IX_FK__OrderAddi__Addit__403A8C7D]
ON [dbo].[OrderAdditionsTBL]
    ([AdditionID]);
GO

-- Creating foreign key on [EventID] in table 'OrdersTBL'
ALTER TABLE [dbo].[OrdersTBL]
ADD CONSTRAINT [FK__OrdersTBL__Event__3B75D760]
    FOREIGN KEY ([EventID])
    REFERENCES [dbo].[EventTypesTBL]
        ([EventID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__OrdersTBL__Event__3B75D760'
CREATE INDEX [IX_FK__OrdersTBL__Event__3B75D760]
ON [dbo].[OrdersTBL]
    ([EventID]);
GO

-- Creating foreign key on [FoodId] in table 'FoodInMealMenuTBL'
ALTER TABLE [dbo].[FoodInMealMenuTBL]
ADD CONSTRAINT [FK__FoodInMea__FoodI__2F10007B]
    FOREIGN KEY ([FoodId])
    REFERENCES [dbo].[FoodsTBL]
        ([FoodID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__FoodInMea__FoodI__2F10007B'
CREATE INDEX [IX_FK__FoodInMea__FoodI__2F10007B]
ON [dbo].[FoodInMealMenuTBL]
    ([FoodId]);
GO

-- Creating foreign key on [MealMenuId] in table 'FoodInMealMenuTBL'
ALTER TABLE [dbo].[FoodInMealMenuTBL]
ADD CONSTRAINT [FK__FoodInMea__MealM__2E1BDC42]
    FOREIGN KEY ([MealMenuId])
    REFERENCES [dbo].[MealInMenuTBL]
        ([MealMenuId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__FoodInMea__MealM__2E1BDC42'
CREATE INDEX [IX_FK__FoodInMea__MealM__2E1BDC42]
ON [dbo].[FoodInMealMenuTBL]
    ([MealMenuId]);
GO

-- Creating foreign key on [FoodInMealMenuID] in table 'OrderDetailsTBL'
ALTER TABLE [dbo].[OrderDetailsTBL]
ADD CONSTRAINT [FK__OrderDeta__FoodI__440B1D61]
    FOREIGN KEY ([FoodInMealMenuID])
    REFERENCES [dbo].[FoodInMealMenuTBL]
        ([FoodInMealMenuID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__OrderDeta__FoodI__440B1D61'
CREATE INDEX [IX_FK__OrderDeta__FoodI__440B1D61]
ON [dbo].[OrderDetailsTBL]
    ([FoodInMealMenuID]);
GO

-- Creating foreign key on [FoodId] in table 'ProductInFoodTBL'
ALTER TABLE [dbo].[ProductInFoodTBL]
ADD CONSTRAINT [FK__ProductIn__FoodI__33D4B598]
    FOREIGN KEY ([FoodId])
    REFERENCES [dbo].[FoodsTBL]
        ([FoodID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__ProductIn__FoodI__33D4B598'
CREATE INDEX [IX_FK__ProductIn__FoodI__33D4B598]
ON [dbo].[ProductInFoodTBL]
    ([FoodId]);
GO

-- Creating foreign key on [MealID] in table 'MealInMenuTBL'
ALTER TABLE [dbo].[MealInMenuTBL]
ADD CONSTRAINT [FK__MealInMen__MealI__29572725]
    FOREIGN KEY ([MealID])
    REFERENCES [dbo].[MealsTBL]
        ([MealID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__MealInMen__MealI__29572725'
CREATE INDEX [IX_FK__MealInMen__MealI__29572725]
ON [dbo].[MealInMenuTBL]
    ([MealID]);
GO

-- Creating foreign key on [MenuID] in table 'MealInMenuTBL'
ALTER TABLE [dbo].[MealInMenuTBL]
ADD CONSTRAINT [FK__MealInMen__MenuI__286302EC]
    FOREIGN KEY ([MenuID])
    REFERENCES [dbo].[MenusTBL]
        ([MenuID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__MealInMen__MenuI__286302EC'
CREATE INDEX [IX_FK__MealInMen__MenuI__286302EC]
ON [dbo].[MealInMenuTBL]
    ([MenuID]);
GO

-- Creating foreign key on [MenuID] in table 'OrdersTBL'
ALTER TABLE [dbo].[OrdersTBL]
ADD CONSTRAINT [FK__OrdersTBL__MenuI__3C69FB99]
    FOREIGN KEY ([MenuID])
    REFERENCES [dbo].[MenusTBL]
        ([MenuID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__OrdersTBL__MenuI__3C69FB99'
CREATE INDEX [IX_FK__OrdersTBL__MenuI__3C69FB99]
ON [dbo].[OrdersTBL]
    ([MenuID]);
GO

-- Creating foreign key on [OrderID] in table 'OrderAdditionsTBL'
ALTER TABLE [dbo].[OrderAdditionsTBL]
ADD CONSTRAINT [FK__OrderAddi__Order__3F466844]
    FOREIGN KEY ([OrderID])
    REFERENCES [dbo].[OrdersTBL]
        ([OrderID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__OrderAddi__Order__3F466844'
CREATE INDEX [IX_FK__OrderAddi__Order__3F466844]
ON [dbo].[OrderAdditionsTBL]
    ([OrderID]);
GO

-- Creating foreign key on [OrderID] in table 'OrderDetailsTBL'
ALTER TABLE [dbo].[OrderDetailsTBL]
ADD CONSTRAINT [FK__OrderDeta__Order__4316F928]
    FOREIGN KEY ([OrderID])
    REFERENCES [dbo].[OrdersTBL]
        ([OrderID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__OrderDeta__Order__4316F928'
CREATE INDEX [IX_FK__OrderDeta__Order__4316F928]
ON [dbo].[OrderDetailsTBL]
    ([OrderID]);
GO

-- Creating foreign key on [ProductID] in table 'ProductInFoodTBL'
ALTER TABLE [dbo].[ProductInFoodTBL]
ADD CONSTRAINT [FK__ProductIn__Produ__34C8D9D1]
    FOREIGN KEY ([ProductID])
    REFERENCES [dbo].[ProductsTBL]
        ([ProductID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__ProductIn__Produ__34C8D9D1'
CREATE INDEX [IX_FK__ProductIn__Produ__34C8D9D1]
ON [dbo].[ProductInFoodTBL]
    ([ProductID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------